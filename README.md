# Computing Kontsevich integrals

This _Maple_ program computes _Kontsevich weights_, which are integrals of hyperbolic angles over configuration spaces of marked discs, in terms of multiple zeta values. The underlying algorithm is combinatorial and uses Francis Brown's theory of _hyperlogarithms_ on the moduli space of marked spheres and the method of _single valued integration_ as promoted by Oliver Schnetz.

## Getting Started

To obtain a copy of this code, either clone the whole repository using
```
git clone https://PanzerErik@bitbucket.org/PanzerErik/kontsevint.git
```
or just download the file [kontsv01.mpl](https://bitbucket.org/PanzerErik/kontsevint/src/master/kontsv01.mpl) (note that this code handles only graphs with `m=2` terrestrial vertices; for more general cases, the file [kontsv.mpl](https://bitbucket.org/PanzerErik/kontsevint/src/master/kontsv.mpl) has to be used instead).

### Prerequisites

The code needs the _HyperInt_ library, which is available [here](https://bitbucket.org/PanzerErik/hyperint). The files `HyperInt.mpl` and `periodLookups.m` (or links to these files) must be in the working directory that holds `kontsv01.mpl`.

If everything works, the following *Maple* session
```
read "kontsv01.mpl":
weight([[L,R]]);
```
should run through without any errors and return `1/2` as output.

## Running the tests

```
maple -q tests.mpl
```

## References

* [Multiple zeta values in deformation quantization](https://arxiv.org/abs/1812.11649): paper that explains the underlying algorithm (by Peter Banks, Erik Panzer and Brent Pym)
* [StarProducts](https://bitbucket.org/bpym/starproducts/src/master/): package to compute star products using these weights
* [Deformation Quantization of Poisson manifolds](https://link.springer.com/article/10.1023/B:MATH.0000027508.00421.bf) - Kontsevich's paper
* [HyperInt](https://arxiv.org/abs/1403.3385) (arXiv), or on [Bitbucket](https://bitbucket.org/PanzerErik/hyperint) - prerequisite

## Author

* **Erik Panzer** <http://people.maths.ox.ac.uk/panzer/>

## License


## Acknowledgments

Many thanks to Brent Pym for discussions of the algorithm, and for Peter Banks for testing the code. Further thanks to Ricardo Buring for additional tests of the computed weights through associativity relations.
