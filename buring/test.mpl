# Erik Panzer
# 19 April 2018
# Code to check Buring's weight relations
#  * first, it loads all basic graphs from a file and finds isomorphic versions in our own result list
#  * then it checks the relations (from another file) with the substitutions by our results

# to perform the checks, run "maple -q test.mpl"; it should run through without errors

interface(errorbreak=2):

for n from 1 to 5 do
	read cat("../weights/harmonic/harmonic_", n, "_2.mpl"):
end do:

# For speedup of graph lookup, hash by degree sequence:
degreeSequence := proc(AL::list(list))
local res, indeg:
	# Format: [{indeg(L),indeg(R)}, sort({indeg(vi)})]
	indeg := proc(a) nops(select(has, map(op, AL), a)): end proc:
	res := [op(sort([indeg(L),indeg(R)])),op(sort([seq(indeg(i), i=1..nops(AL))]))]:
	if `+`(op(res))<>2*nops(AL) then
		print(res,AL):
		error "indegrees do not sum up":
	end if:
	return res:
end proc:

toEdgeList := proc(AL::list(list))
	[seq(op([[i, AL[i][1]],[i, AL[i][2]]]), i=1..nops(AL))]:
end proc:

toMapleGraph := proc(AL::list(list))
uses GraphTheory:
	Graph({op(toEdgeList(AL))}):
end proc:

hashGraph := proc(AL::list(list))
	[op(degreeSequence(AL)), GraphTheory[Girth](GraphTheory[UnderlyingGraph](toMapleGraph(AL)))]:
end proc:

byHash := table():
for key in indices(w,'nolist') do
	hash := hashGraph(key):
	if not assigned(byHash[hash]) then
		byHash[hash] := {key}:
	else
		byHash[hash] := byHash[hash] union {key}:
	end if:
end do:

permParity := proc(a::list, b::list)
	if {op(a)}<>{op(b)} then
		error "not a permutation":
	elif nops(a)<>nops({op(a)}) then
		error "not a permutation (repeated elements)":
	end if:
	GroupTheory[PermParity](Perm([seq(ListTools[Search](a[i], b), i=1..nops(a))])):
end proc:

# AL=adjacency list (outgoing edges of the internal vertices)
findGraph := proc(AL::list(list))
uses GraphTheory:
local needle, g, G, phi, DS:
global byDS:
	needle := toMapleGraph(AL):
	hash := hashGraph(AL):
#	for g in indices(w, 'nolist') do
	for g in byHash[hash] do
		G := toMapleGraph(g):
		if IsIsomorphic(G,needle,'phi') then
			# determine relative sign:
			return 
				w[g]
				*permParity(eval(toEdgeList(g), phi), toEdgeList(AL))
				*`if`(eval(L,phi)=R, (-1)^nops(AL), 1) # flip L<->R
		end if:
	end do:
	error "not found!":
end proc:

# read Buring's graphs:
f := fopen("buring_graphs.txt", READ, TEXT):
line := readline(f): # skip first line
VALS := table():
while true do
	line := readline(f):
	if line=0 then break: end if:
	tokens := sscanf(line, "%d %d %d"):
	if nops(tokens)<>3 then
		Warning("skipping line: ", line):
		next:
	end if:
	m,n,s := op(tokens):
	if m<>2 then
		error "expected m=2, got m=%1", m:
	end if:
	if (n<0) or (n>6) then
		error "n=%1 out of range (0..6)", n:
	end if:
	fmt := cat("%d "$(3+2*n), "%a"):
	tokens := sscanf(line, fmt):
	if nops(tokens)<>4+2*n then
		error "incomplete graph specification in line: %1", line:
	end if:
	val := tokens[-1]:
	# 0=L, 1=R, 2...n+2=internal vertices
	tokens := tokens[4..-2]:
	tokens := subs({0='L',1='R',seq(i+1=i,i=1..n)}, tokens):
	g := [seq(tokens[2*i-1..2*i], i=1..n)]:
	# find graph:
	known := findGraph(g):
	if type(val, 'numeric') then
		if val<>known then
			error "%1=%2: wrong value (%3)", g, known, val:
		else
			printf("%a=%a (correct)\n", g, val):
		end if:
	else
		printf("%a=%a (from %a)\n", val, known, g):
		VALS[val] := known:
	end if:
end do:
fclose(f):

SUBS := [seq(val=VALS[val], val in indices(VALS, 'nolist'))]:

printf("Now checking relations:\n"):
f := fopen("buring_relations.txt", READ, TEXT):
iLine := 0:
while true do
	iLine := iLine+1:
	line := readline(f):
	if line=0 then break: end if:
	tokens := sscanf(line, "%[^=]==%[^=]"):
	if nops(tokens)<>2 then
		Warning("skipping line: ", iLine):
		next:
	end if:
	EQ := parse(tokens[1])=parse(tokens[2]):
	print(EQ):
	printf("checking line %d: ", iLine):
	if not simplify(eval(EQ, SUBS)) then
		error "wrong result: %1", simplify(eval(EQ, SUBS)):
	else
		printf("OK\n"):
	end if:
end do:
fclose(f):

