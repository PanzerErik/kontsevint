# Erik Panzer
# started 14 July 2016

# Code to evaluate the weights in Kontsevich's formula
# based on single-valued integration

# branched on 29 July 2017: integrate out 1, only keep 0

zeta[2] := Zeta(2):

interface(errorbreak=2):
read "HyperInt.mpl":
_hyper_check_divergences := false:

COMBINE_ITERRES := true:
WEDGE_SIMPLIFY  := true:

WEDGE_CHECK_SAMPLES := 10:

# Do not want normal() on each table access!

`index/sparsereduced` := proc(Idx::list, Tbl::table, Entry::list)
local val:
	if (nargs = 2) then
		if assigned(Tbl[op(Idx)]) then Tbl[op(Idx)]:
		else 0: end if:
	else
#		val := Entry[1]:
		val := expand(Entry[1]):
		if val = 0 then
			Tbl[op(Idx)] := evaln(Tbl[op(Idx)]):
		else
			Tbl[op(Idx)] := val:
		end if:
		val:
	end if:
end proc:

forgetAllSV := proc()
	forget(dlogSimplify,wedgeSimplify,dlogsIterRes,Reduction,wedgeRes,axisHlog,zeroInfPeriodOver1,HlogReglimList,fibBasisList,transportTo1,transportToInf):
end proc:

# Only computes the iterated residues, i.e. assumes that all higher poles (and polynomial parts) cancel
iterRes := proc(expr, vars::list)
local res, S, i, p, X, Z, N, P, Q, f, zero, s, pre, z, had:
	if expr=0 then
		return 0:
	elif op(0,expr)=`+` then
		return map(iterRes, expr, vars):
	elif vars=[] then
		return normal(expr):
	end if:
	# Find zeros of denominator
	if op(0,expr)=DEN then
		pre, Z := 1, [expr]:
	elif op(0,expr)=`*` then
		Z, pre := selectremove(w->op(0,w)=DEN, [op(expr)]):
	else
		error "unexpected argument: %1", expr:
	end if:
	Z, Q := selectremove(has, Z, vars[1]):
	pre := `*`(op(pre),op(Q)):
	res := 0:
	Z := map(op, Z):
	had := []:
	for i from 1 to nops(Z) do
		z := -coeff(Z[i], vars[1], 0)/coeff(Z[i], vars[1], 1):
		if z in had then
			error "higher order pole":
		end if:
		had := [op(had), z]:
		X := mul(`if`(j=i, 1/coeff(Z[i], vars[1], 1), DEN(eval(Z[j], vars[1]=z))), j=1..nops(Z)):
		res := res + den(vars[1],z)*iterRes(X*pre, vars[2..]):
	end do:
	return res:
end proc:

conj := proc(expr)
	if op(0,expr)=z then
		zz[op(1,expr)]:
	elif op(0,expr)=zz then
		z[op(1,expr)]:
	else
		conjugate(expr):
	end if:
end proc:

# continuation around var=at and evaluation at zero
HlogLooped := proc(at, w::list, val:=2*Pi*I)
local i, j, res, u:
	if w=[] then
		return 1:
	end if:
	res := 0:
	# loop over subsequences of "at" starting at i and ending at j
	for i from 1 to nops(w) do
		for j from i to nops(w) do
			if w[j]<>at then
				break:
			end if:
			res := res
			+add((-1)^nops(u[2])*Hlog(at, ListTools[Reverse](u[2]))*u[1], u in regtail([[1, w[..i-1]]], at, 0))
			*val^(j-i+1)/(j-i+1)!
			*add(Hlog(at, u[2])*u[1], u in reghead([[1, w[j+1..]]], at, 0)):
		end do:
	end do:
	res:
end proc:

# Hlog after continuation around var=at
HlogContinued := proc(var, w::list, at, val:=2*Pi*I)
	add(`if`(i=0, 1, Hlog(var, w[1..i]))*HlogLooped(at, w[i+1..], val), i=0..nops(w)):
end proc:

# only for functions of zz
monodromy := proc(expr::table, at, res::table)
global allvars:
local z, w, T, X:
	z := allvars[1]:
	printf("Computing monodromy around %a\n", conj(z)=conj(at)):
	for w in indices(expr, 'nolist') do
		X := HlogContinued(conj(z), w[1], conj(at), -2*Pi*I) - Hlog(conj(z), w[1]):
		T := table(sparsereduced):
		fibrationBasis(X, allvars[2..], T):
		fibrationBasisProduct(T, [[], op(w[2..])], res, expr[w]):
	end do:
end proc:

# expr depends on z and zz
# !ASSUMES! that the monodromy is independent of z (analytic in zz)
doubleMonodromy := proc(expr::table, at, res::table)
local z, X, Y, w:
global allvars:
	z := allvars[1]:
	printf("Computing monodromy around %a\n", z=at):
	res := table(sparsereduced):
	for w in indices(expr, 'nolist') do
		X := HlogLooped(at, w[1], 2*Pi*I)*HlogContinued(conj(z), w[2], conj(at), -2*Pi*I):
		if w[1]=[] then
			X := X-Hlog(conj(z), w[2]):
		end if:
		if X=0 then next: fi:
		Y := table(sparsereduced):
		fibrationBasis(X, allvars[2..], Y):
		fibrationBasisProduct(Y, [[], op(w[3..])], res, expr[w]):
	end do:
end proc:

halfRes := proc(expr::table, eq::equation, result::table)
local z, at, X, H, p, q, hlog, k, onto, T, Y, u, w, pre, v, weight, ww, WW, W, S, P:
global allvars:
	z, at := op(eq):
	if not (at in {0,infinity}) then
		error "half-residue at %1 not implemented", at:
	end if:
	if at=infinity then
		S := {indices(expr, 'nolist')}:
#		onto := table(sparsereduced):
#		for w in S do
#			onto[w] := eval(expr[w], den=((u,v)->`if`(u=conj(z), -1, den(u,v)))):
#		end do:
#		lprint([entries(onto,'nolist')]):
#		print(numelems(expr), VS, numelems(onto)):
		X := table(sparsereduced):
		for w in map2(op,1,S) do
			T := table(sparsereduced, transportToInf(w, allvars)):
			W := select(v->v[1]=w, S):
			for ww in map2(op,2,W) do
				H := table(sparsereduced):
				for u in transportToInf(ww, allvars[2..]) do
					fibrationBasisProduct(T, [[], op(lhs(u))], H, rhs(u)):

				end do:
		
				WW := select(v->v[2]=ww, W):
				for u in WW do
					# determine prefactor
					pre := selectfun(expr[u], den):
					pre := select(u->op(1,u)=conj(z), pre):
					# residues at infinity=-sum of all residues at finite points, because 1/z^2/(-1/z-s)=1/z/(-1-s*z) has residue -1 at z=0
					pre := subs(seq(v=-1, v in pre), expr[u])*den(conj(z),0):
					if pre = 0 then next: fi:
					fibrationBasisProduct(H, [[], [], op(u[3..])], X, pre):
				end do:
			end do:
		end do:
		halfRes(X, allvars[1]=0, result):
		return:
	end if:
	# Now in case at=0
	for w in {indices(expr, 'nolist')} do
		if not ({op(w[1]), op(w[2])} subset {0,conj(z)}) then
			next:
		end if:
		pre := coeff(expr[w], den(conj(z), conj(at)), 1):
		if pre = 0 then next: fi:
		if w[1]<>[] then
			printf("Angle-dependent function at the half-residue:\n"):
			print(Hlog(z, w[1])*Hlog(conj(z), w[2])):
			error "divergent term":
		end if:

		k := nops(w[2])+1:
		q := -(-Pi*I)^k/k!:

		result[w[3..]] := result[w[3..]] + pre*q:
	end do:
end proc:

lastInt := proc(expr::table, result::table)
local z, at, X, H, p, q, hlog, k, onto, T, Y, u, w, pre, v, weight, ww, WW, W, S, P:
global allvars:
	z := allvars[1]:
	for w in {indices(expr, 'nolist')} do
#if w[2]<>[] then next: fi:
		if not ({op(w[1]), op(w[2])} subset {0,conj(z)}) then
			error "unexpected letter in %1", w:
		end if:
		pre := 1:
#		pre := coeff(expr[w], den(conj(z), 0), 1):
#		if pre = 0 then next: fi:

		u := eval(w[1],conj(z)=1):

		q := 0:
		for k from nops(u) to 1 by -1 do
			if u[k]<>0 then
				break:
			end if:
			q := q+1:
		end do:

#		u := [[1, u]]:

		H := regtail([[(-TT/2)^nops(w[2])/(nops(w[2])!),u]], 0, TT/2):

		u := []:
		q := q + nops(w[2]):
		for k from 0 to q do
			u := [op(u), op(shuffle(map(p->[coeff(p[1], TT, k), p[2]], H), [[k!, [0$k]]]))]:
		end do:


		# trailing zeros in w[2] = log(zz) = -1/2 log(z/zz) on unit circle
#		pre := pre*(-1/2)^nops(w[2]):
		# write as iterated integral in z/zz:
#		u := shuffle([[1, u]], [[1, w[2]]]):

		u := [
			seq([k[1]*(-coeff(expr[w], den(conj(z), 0), 1)-coeff(expr[w], den(conj(z), z), 1)), [0,op(k[2])]], k in u),
			seq([k[1]*coeff(expr[w], den(conj(z), z), 1), [1,op(k[2])]], k in u)
		]:

		weight := nops(w[1])+nops(w[2])+1:
		pre := pre/2:
		# monodromy; k=number of trailing zeros that are clipped off
		q := 0:
		for p in u do
			if p[1]=0 then next: fi:
			if nops(p[2])<>weight then
				error "wrong weight":
			end if:
			for k from 1 to weight do
				if p[2][weight+1-k]<>0 then
					break:
				end if:
				q := q + (2*Pi*I)^k/k!*p[1]*zeroOnePeriod(p[2][1..(weight-k)]):
			end do:
		end do:

		result[w[3..]] := result[w[3..]] + pre*q:
	end do:
end proc:

svRes := proc(expr::table, eq::equation, res::table)
local z, at, X, pre, w, T:
global allvars:
	z, at := op(eq):
	if at in {0,infinity} then
		error "svRes should only be taken at other variables in the upper half plane; not at %1", at:
	end if:
	for w in indices(expr, 'nolist') do
		pre := coeff(expr[w], den(conj(z), conj(at)), 1):
		if pre=0 then next: end if:
		X := add(f[1]*Hlog(at, f[2]), f in reghead([[1,eval(w[1],conj(z)=conj(at))]], at, log(-1/at))):
		X := X*add(f[1]*Hlog(conj(at), f[2]), f in reghead([[1,w[2]]], conj(at), log(-1/conj(at)))):
		T := table(sparsereduced):
		fibrationBasis(X, allvars[3..], T):
		fibrationBasisProduct(T, w[3..], res, pre*(2*Pi*I)):
	end do:
end proc:

# Observation: If the only var-dependent letter in word is var, then the fibrationBasis of Hlog(var, word)  has only Hlogs of var. This is because the reglim_var->0 of such an object is zero whenever word contains a letter different from 0 or z (rescaling as Hlog(1, word_s->s/z) gives something with vanishing limit z->0 in this case)
# If the only letters are 0 and z, then Hlog(z,word) is just an MZV
# So the axis limit only transforms Hlog(z,) and Hlog(zz,)'s, but in doing so does NOT introduce further Hlog's of other variables
axisHlog := proc(var, word::list)
option remember:
global allvars:
local u, T:
	if not (allvars[2] in word) then
		[[1, word]]:
	end if:
	T := table(sparsereduced):
	fibrationBasis(add(u[1]*Hlog(var, u[2]), u in reghead([[1,eval(word, conj(var)=var)]], var, 0)), [var], T):
	[seq([eval(rhs(u), zeta[2]=Zeta(2)), lhs(u)[1]], u in indices(T, 'pairs'))]:
end proc:

axisLimit := proc(expr::table, z, result::table)
local w, S, SS, T, u:
global allvars:
	S := [indices(expr, 'nolist')]:
	SS := {op(map2(op, 1, S))}:
	for w in SS do
		T := table(sparsereduced):
		for u in select(f->f[1]=w, S) do
			T[u[2..]] := eval(expr[u], conj(z)=z):
		end do:
		for u in axisHlog(z, w) do
			fibrationBasisProduct(T, [u[2], []$(nops(allvars)-2)], result, u[1]):
		end do:
	end do:
end proc:

HlogsIntoTable := proc(expr, vars::list, tab::table, pre:=1)
local todo, Hs, ws, v, H, con:
	if op(0,expr)=`+` then
		map(HlogsIntoTable, [op(expr)], vars, tab, pre):
		return:
	end if:
	if op(0,expr)=`*` then
		todo := [op(expr)]:
	else
		todo := [expr]:
	end if:
	Hs, todo := selectremove(f->op(0,f)=Hlog, todo):
	# constants:
	todo,con := selectremove(has, todo, Hlog):
	con := `*`(op(con)):
	if (Hs<>[]) and (todo<>[]) then
#		WARNING("Expanding expression to extract different Hlogs\n"):
		error "Expanding expression to extract different Hlogs\n":
		HlogsIntoTable(expand(expr), vars, tab, pre):
		return:
	end if:
	if nops(todo)=1 then
		# Now Hs=[], so we just have a prefactor
		HlogsIntoTable(todo[1], vars, tab, pre*con):
		return:
	end if:
	if nops(todo)>1 then
#		WARNING("Expanding expression to extract different Hlogs\n"):
		error "Expanding expression to extract different Hlogs\n":
		HlogsIntoTable(expand(`*`(op(todo))), vars, tab, pre*con):
		return:
	end if:
	ws := [[]$nops(vars)]:
	for H in Hs do
		if op(2,H)=[] then
			next:
		end if:
		v := op(1, H):
		v := ListTools[Search](v, vars):
		if v=0 then
			error "unknown variable: %1", op(1,H):
		end if:
		if ws[v]<>[] then
			error "several Hlog-factors for the same variable %1 in %2", vars[v], Hs:
		end if:
		ws[v] := op(2,H):
	end do:
	tab[ws] := tab[ws] + con*pre:
end proc:

partialFractions := proc(expr, var)
local res, S, w:
	S := selectfun(expr, den):
	S := select(w->op(1,w)=var, S):
	res := [0, seq([op(2,w), coeff(expr,w,1)], w in S)]:
	return res:
end proc:

subtract_last_realaxis := false:

# computes a single-valued primitive wrt allvars[1]
svPrim := proc(expr)
local words, w, S, Mon, X, maxWeight, W, Y, z, tab, Prim, u, v:
global allvars, delta, subtract_last_realaxis:

	z := allvars[1]:
	if conj(z)<>allvars[2] then
		error "expecting second variable to be %1, the conjugate of %1, but found %1", conj(z), z, allvars[2]:
	end if:

	tab := table(sparsereduced):
	HlogsIntoTable(expr, allvars, tab):

	# naive primitive (not single-valued):
	Prim := table(sparsereduced):
	for w in indices(tab, 'nolist') do
		Y := tab[w]:
		W := selectfun(Y, den):
		W := select(u->op(1,u)=z, W):
		for u in W do
			v := [[op(2,u), op(w[1])], op(w[2..])]:
			X := coeff(Y, u, 1):
			Prim[v] := Prim[v] + X:
		end do:
	end do:


	printf("Naive primitive has %d terms\n", numelems(Prim)):



	# Subtract real axis limit (at least in last integration):
	if (nops(allvars)=2) and (subtract_last_realaxis) then
		printf("subtracting real axis limit on (0,1):\n"):
		tab := table(sparsereduced):
		axisLimit(Prim, z, tab):
		for w in [indices(tab,'nolist')] do
			X := [[], op(w)]:
			Prim[X] := Prim[X] - eval(tab[w], allvars[1]=allvars[2]):
		end do:
		printf(" - primitive after subtraction: %d (%d terms)\n", max(0,seq(`+`(map(nops,w)), w in [indices(Prim,'nolist')])), numelems(Prim)):
	end if:



	# letters in z:
	words := {seq(op(w[1]), w in indices(Prim, 'nolist'))}:
	if conj(z) in words then
		printf("z-zz occured\n", u):
	end if:
	# in the upper half-plane:
	S := remove(v->delta[v]=-1, words):
	S := (S intersect {op(allvars)}) union select(v->Im(v)>0, S minus {op(allvars)}):
	printf("singular points of primitive in the upper half plane: %a\n", S):
	printf("singularities not in the upper half plane: %a\n", words minus S):

	# compute monodromies
	for w in S do
		Mon[w] := table(sparsereduced):
		doubleMonodromy(Prim, w, Mon[w]):
		if numelems(Mon[w])=0 then
			maxWeight[w] := -1:
		else
			maxWeight[w] := max({seq(nops(w[1]), w in [indices(Mon[w], 'nolist')])}):
		end if:
		printf(" - monodromy at %a of weight %a\n", z=w, maxWeight[w]):
	end do:

	# Important: In the following step, we add monodromies to make the primitive single-valued
	# These monodromies can have new monodromies, where the original (non-sv. primitive) did not have any!
	# example: suppose Prim has only z[1]-Hlog's of the form Hlog(z[1], {0,zz[1],z[2],zz[3]}*), hence the only possible monodromy in the upper half plane is at z[1]=z[2]
	# put when we fix this monodromy, we add integrals like Hlog(z[2], {0,zz[1],z[2],zz[3]}*) which transform into Hlog(zz[1], {0,z[2],zz[3]}*) in the fibration basis.
	# these functions can then have monodromies at z[1]=z[3], which need to be subtracted later on!
	words := select(v->delta[v]>0, {op(allvars[3..])}) minus S:
	printf("further points in the upper half-plane: %a\n", words):
	for w in words do
		Mon[w] := table(sparsereduced):
		maxWeight[w] := -1:
	end do:
	S := S union words:
	



	# fix monodromies, highest weight to lowest
	if numelems(S)=0 then
		W := -1:
	else
		W := max(entries(maxWeight, 'nolist')):
	end if:
	while W>=0 do
		tab := table(sparsereduced):
		for w in S do
			for u in [indices(Mon[w], 'nolist')] do
				if nops(u[1])<W then next: end if:
				X := [[op(u[1]), conj(w)], op(u[2..])]:
				tab[X] := -Mon[w][u]/(-2*Pi*I):
				X := [[], op(X)]:
				Prim[X] := Prim[X]-Mon[w][u]/(-2*Pi*I):
			end do:
		end do:
		for w in S do
			monodromy(tab, w, Mon[w]):
			if numelems(Mon[w])=0 then
				maxWeight[w] := -1:
			else
				maxWeight[w] := max({seq(nops(w[1]), w in [indices(Mon[w], 'nolist')])}):
			end if:
			printf(" - monodromy at %a of weight %a\n", z=w, maxWeight[w]):
		end do:
		W := W-1:
	end do:
	# Finished computation of single-valued primitive
	printf("Single-valued primitive has %d terms\n", numelems(Prim)):
	return copy(Prim):
end proc:

# Reg_{z->infinity} Hlog(z, word)
# DELTA=signum(Im(z))
zeroInfPeriodOver1 := proc(word::list({0,1}))
local w, n, i:
option remember:
	if word=[] then
		1:
	else
		n := nops(word):
		subs(seq(DELTA^(2*i)=1, i=1..floor(n/2)),seq(DELTA^(2*i+1)=DELTA, i=1..floor((n-1)/2)),add(
			expand(w[1]*zeroInfPeriod(subs([0=-1,1=0], w[2])))
		,w in regtail([seq([zeroOnePeriod(word[i+1..]), word[..i]], i=0..n)], 1, -I*Pi*DELTA))):
	end if:
end proc:

# Reglim_{wrt->0} Hlog(var, word)
HlogReglim := proc(at, word::list, wrt)
local depLet, head, v, n, post, first, last, pre, tail:
global delta:
	if word=[] then
		return 1:
	elif word[1]=at then
		error "divergent Hlog(%1, %2)", at, word:
	elif (at=1) and ({op(word)} subset {0,1}) then
		return zeroOnePeriod(word):
	elif (at=infinity) and ({op(word)} subset {0,-1}) then
		return zeroInfPeriod(word):
	elif (at=infinity) and (word[-1]=0) then
		return add(v[1]*HlogReglim(at, v[2], wrt), v in regtail([[1, word]], 0, 0)):
	end if:
	# letters dependent on wrt:
	depLet := select(has, {op(word)}, wrt):
	if (depLet={}) and (at=-1/wrt) then
		pre := regtail([[1, map(v->-v, word)]], 0, I*Pi*delta[wrt]):
		return add(v[1]*`if`({op(v[2])} subset {-1,0}, zeroInfPeriod(v[2]), Hlog(infinity, v[2])), v in pre):
	end if:
	if depLet={} then
		if eval(at, wrt=0)=0 then
			return 0:
		else
			return Hlog(eval(at, wrt=0), word):
		end if:
	end if:
	if (depLet={wrt}) and (at=1) then
		# shuffle of the longest tail in {0,wrt}
		tail := 0:
		n := nops(word):
		while tail<n do
			if word[n-tail] in {0,wrt} then
				tail := tail+1:
			else
				break:
			end if:
		end do:
		if tail=0 then
			pre := eval(word, wrt=0):
			if {op(post)} subset {0,1} then
				return zeroOnePeriod(post):
			else
				return Hlog(at, pre):
			end if:
		elif tail=n then
			return subs(DELTA=-delta[wrt], zeroInfPeriodOver1(subs(wrt=1, word))):
		end if:
		pre := eval(word[..n-tail-1], wrt=0):
		last := word[n-tail]:
		if {last, op(pre)} subset {0,1} then
			return add(subs(DELTA=-delta[wrt], zeroInfPeriodOver1(subs(wrt=1, word[n-i+1..])))*zeroOnePeriod([op(pre), last, 0$(tail-i)]), i=0..tail):
		else
			return add(subs(DELTA=-delta[wrt], zeroInfPeriodOver1(subs(wrt=1, word[n-i+1..])))*add(v[1]*Hlog(1, [op(v[2]), last]),  v in shuffle([[1, pre]], [[(-1)^(tail-i), [0$(tail-i)]]])), i=0..tail):
		end if:
	elif (depLet={1+wrt}) and (at=1) then
		# This is the case occurring from the expansion at z=1+u, which gives periods int_0^1 {...,1+uu} from the path concatenation
		# shuffle of the longest prefix in {1,1+wrt}
		head := 0:
		n := nops(word):
		while head<n do
			if word[head+1] in {1,1+wrt} then
				head := head+1:
			else
				break:
			end if:
		end do:
		if head=0 then
			post := eval(word, wrt=0):
			if {op(post)} subset {0,1} then
				return zeroOnePeriod(post):
			else
				return Hlog(at, post):
			end if:
		elif head=n then
			return (-1)^n*zeroInfPeriod([seq(`if`(word[n-i]=1, 0, -1), i=0..n-1)]):
		end if:
		post := eval(word[head+2..], wrt=0):
		first := word[head+1]:
		if {first, op(post)} subset {0,1} then
			# in this case here, first=0 and the shuffle regularization is already computed in the lookup tables
			return add((-1)^i*zeroInfPeriod([seq(`if`(word[i-j]=1,0,-1), j=0..i-1)])*zeroOnePeriod([1$(head-i), first, op(post)]), i=0..head):
		else
			return add((-1)^i*zeroInfPeriod([seq(`if`(word[i-j]=1,0,-1), j=0..i-1)])*add(v[1]*Hlog(1, [first, op(v[2])]),  v in shuffle([[1, post]], [[(-1)^(head-i), [1$(head-i)]]])), i=0..head):
		end if:
	else
		error "HlogReglim(%1,%2,%3) not implemented", at, word, wrt:
	end if:
end proc:

# d/dwrt log((z-before)/(z-after))
prependList := proc(z, after, before, wrt)
local f, res:
	res := 0:
	f := simplify(z-before):
	if degree(f,wrt)>1 then
		error "nonlinear polynomial %1 unexpected", f:
	elif degree(f, wrt)=1 then
		res := dlog(factor(-coeff(f,wrt,0)/coeff(f,wrt,1))):
	end if:
	f := simplify(z-after):
	if degree(f,wrt)>1 then
		error "nonlinear polynomial %1 unexpected", f:
	elif degree(f, wrt)=1 then
		res := res - dlog(factor(-coeff(f,wrt,0)/coeff(f,wrt,1))):
	end if:
	return [seq([coeff(res,f,1), op(f)], f in selectfun(res, dlog))]:
end proc:

# prepend Hlog(wrt, w)'s in expr with L=[[n1,z1],...] -> n1*Hlog(wrt, [z1,w])+n2*...
prependHlogs := proc(expr, L::list, wrt)
local Hlogs:
	if L=[] then
		return 0:
	else
		Hlogs := select(h->op(1,h)=wrt, selectfun(expr, Hlog)):
		return add(coeff(expr, h)*add(v[1]*Hlog(wrt, [v[2], op(op(2, h))]), v in L), h in Hlogs) +
		subs(seq(h=0, h in Hlogs), expr)*add(v[1]*Hlog(wrt, [v[2]]), v in L):
	end if:
end proc:

HlogReglimList := proc(var, word, wrt)
local res, X, w, S, v:
option remember:
	X := expand(HlogReglim(var, word, wrt)):
	res := table(sparsereduced):
	if op(0,X)=`+` then
		X := [op(X)]:
	else
		X := [X]:
	end if:
	for w in X do
		if not has(w, Hlog) then
			res[1,[]] := res[1,[]] + w:
			next:
		end if:
		S := selectfun(w, Hlog):
		if nops(S)<>1 then
			error "several or no Hlog's: %1", w:
		end if:
		S := op(S):
		res[op(S)] := res[op(S)] + coeff(w, S, 1):
	end do:
	return [entries(res, 'pairs')]:
end proc:
		

fibBasis := proc(var, word, wrt::list)
local i, n, res, L:
	return add(rhs(w)*mul(`if`(lhs(w)[i]=[], 1, Hlog(wrt[i], lhs(w)[i])), i=1..nops(wrt)), w in fibBasisList(var, word, wrt)):


	if word=[] then
		return 1:
	elif word[1]=var then
		error "divergent Hlog":
	elif (var=1) and ({op(word)} subset {0,1}) then
		return zeroOnePeriod(word):
	elif wrt=[] then
		return Hlog(var, word):
	elif not (has(var, wrt[1]) or has(word, wrt[1])) then
		return fibBasis(var, word, wrt[2..]):
	elif (var=wrt[1]) and (not has(word, wrt[1])) then
		return Hlog(var, word):
	end if:
	# differentiate under the integral and integrate:
	n := nops(word):
	res := fibBasis(HlogReglim(var, word, wrt[1]), wrt[2..]):
	for i from 1 to n do
		L := prependList(word[i], `if`(i=n, 0, word[i+1]), `if`(i=1, var, word[i-1]), wrt[1]):
		if L=[] then
			next:
		end if:
		res := res + 
			prependHlogs(fibBasis(var, subsop(i=NULL, word), wrt), L, wrt[1]):
	end do:
	return collect(res, Hlog):
end proc:


# returns a list of key=value pairs (table initializer)
# key = [w1,...,w_nops(wrt)] corresponds to Hlog(wrt[1], w1)*Hlog(wrt[2], w2)*...
fibBasisList := proc(var, word, wrt::list)
local i, n, res, L, v, w, u:
option remember:
	if word=[] then
		return [[[]$nops(wrt)]=1]:
	elif word[1]=var then
		error "divergent Hlog":
	elif (var=1) and ({op(word)} subset {0,1}) then
		res := zeroOnePeriod(word):
		if res=0 then
			return []:
		else
			return [[[]$nops(wrt)]=res]:
		end if:
	elif wrt=[] then
		return [[] = Hlog(var, word)]:
	elif not (has(var, wrt[1]) or has(word, wrt[1])) then
		return map(v->[[], op(lhs(v))]=rhs(v), fibBasisList(var, word, wrt[2..])):
	elif (var=wrt[1]) and (not has(word, wrt[1])) then
		return [[word, []$(nops(wrt)-1)] = 1]:
	end if:

	n := nops(word):
	res := table(sparsereduced):

	# constant pf integration:
	for w in HlogReglimList(var, word, wrt[1]) do
		for v in fibBasisList(lhs(w), wrt[2..]) do
			u := [[], op(lhs(v))]:
			res[u] := res[u] + rhs(v)*rhs(w):
		end do:
	end do:

	# differentiate under the integral and integrate:
	for i from 1 to n do
		L := prependList(word[i], `if`(i=n, 0, word[i+1]), `if`(i=1, var, word[i-1]), wrt[1]):
		if L=[] then
			next:
		end if:
		for w in fibBasisList(var, subsop(i=NULL, word), wrt) do
			for v in L do
				u := [[v[2], op(lhs(w)[1])], op(lhs(w)[2..])]:
				res[u] := res[u] + v[1]*rhs(w):
			end do:
		end do:
	end do:
	return [indices(res, 'pairs')]:
end proc:

fibBasisListTest := proc(var, word, wrt::list)
local IST, SOLL:
	IST := fibBasis(var, word, wrt):
	SOLL := fibrationBasis(Hlog(var, word), wrt):
	if simplify(IST-SOLL)<>0 then
		error "mismatch for fibBasisList(%1, %2, %3)", var, word, wrt:
	end if:
	return fibBasisList(var, word, wrt):
end proc:

# Write Hlog(1+z, word) in fibration basis wrt vars
#		Path concatenation to transform Hlog(1+z, ...) into Hlog(z, ...):
#
#				Hlog(z, map(q->`if`(q=conj(z), conj(z), q-1), w[1][..i]))
#				*(-I*Pi*delta[z])^k/k!
#				*Hlog(1, Reg_0^1 (w[j+1..]))
transportTo1 := proc(word::list, vars::list)
global delta:
local i, j, v, ww, k, X, u, T, z:
option remember:
	z := vars[1]:
	T := table(sparsereduced):
	# sum over tails: Hlog(1, Reg^1(...))
	for j from 1 to nops(word)+1 do
	for v in reghead([[1, word[j..]]], 1, 0) do
		for ww in fibBasisList(1, subs(conj(z)=conj(z)+1,v[2]), vars[2..]) do
			# sum over {1}*-tails word[..i],{1}* of the prefix word[..j-1]
			for i from j-1 to 0 by -1 do
				if i+1<j then
					if word[i+1]<>1 then
						break:
					end if:
				end if:
				k := j-i-1:
				X := (-I*Pi*delta[z])^k/k!*rhs(ww)*v[1]:
				u := [map(q->`if`(q=conj(z),conj(z),q-1), word[..i]), op(lhs(ww))]:
				T[u] := T[u] + X:
			end do:
		end do:
	end do:
	end do:
	return [entries(T, 'pairs')]:
end proc:

# Expand after z->-1/z, zz->-1/zz
transportToInf := proc(word::list, vars::list)
global delta:
local i, v, u, z, res, pre, n:
option remember:
	z := vars[1]:

	pre := [[1, []]]:
	n := nops(word):

	res := table(sparsereduced):

	for i from 0 to n do
		if i=0 then
			pre := [[1, []]]:
		else
			pre := Mul(pre, [[-1, [0]], `if`(word[i]=conj(z), [1,[conj(z)]], NULL)]):
		end if:
		fibrationBasis(map(u->[u[1],`if`(u[2]=[],[],[u[2]])], regtail([[1, map(v->-v, subs(conj(z)=-1/conj(z), word[i+1..]))]], 0, I*Pi*delta[z])), vars[2..], res, table(), [pre]):
	end do:
	return [entries(res, 'pairs')]:
end proc:

# integrates expr wrt. allvars[1..2]=[z,zz] over the upper half-plane
svInt := proc(expr)
local words, w, S, Mon, X, maxWeight, W, prim, Y, T, z, tab, k, j, resTab, Prim, u, v, i, ww:
global allvars, delta:
	z := allvars[1]:
	if allvars[2]<>conj(z) then
		error "expected %1 as second variable, but got %2", conj(z), allvars[2]:
	end if:

	Prim := svPrim(expr):
	# Prim=fibrationBasis-table of the s.v. primitive

	# Table to store the result of the line integral of Prim:
	resTab := table(sparsereduced):

	# S=poles in the upper half plane:
	S := selectfun({entries(Prim, 'nolist')}, den):
	S := map2(op,2,select(q->op(1,q) = allvars[2], S)):
	S := select(q->delta[q]<0, S intersect {op(allvars)}) union select(q->Im(q)<0, S minus {op(allvars)}):
	S := map(conj, S):
	printf("Poles in the upper half plane: %a\n", S):
	# Compute residues at poles:
	for w in S do
		printf("Residue at %a:\n", z=w):
		svRes(Prim, z=w, resTab):
	end do:

	for w in {0,infinity} do
		printf("Half-residue at %a:\n", z=w):
		halfRes(Prim, z=w, resTab):
	end do:

	# Compute integral along the real line

# TODO: Bring back subtraction!
#	tab := table(sparsereduced):
#	if not ((nops(allvars)=2) and (subtract_last_realaxis)) then
#		axisLimit(Prim, z, tab):
#	end if:


	printf("line integral 0->-infinity:\n"):
	# Analytic continuation around zero is trivial; only the logs (trailing zeros) get I*Pi*delta[z]:

	X := table(sparsereduced):
	for w in indices(Prim, 'nolist') do
		for i from 0 to nops(w[1]) do
			if (i>0) then
				if w[1][-i]<>0 then break: fi:
			end if:
		for j from 0 to nops(w[2]) do
			if (j>0) then
				if w[2][-j]<>0 then break: fi:
			end if:
			ww := [map(f->-f,eval(w[1][..-1-i], conj(z)=-conj(z))), map(f->-f,w[2][..-1-j]), op(w[3..])]:
			X[ww] := X[ww] + eval(Prim[w], den=((aa,bb)->`if`(aa=conj(z), -den(conj(z), -bb), den(aa,bb))))*(I*Pi*delta[z])^i/i!*(I*Pi*delta[conj(z)])^j/j!:
		end do:
		end do:
#		X[w] := X[w] + Prim[w]:
	end do:
	tab := table(sparsereduced):
	axisLimit(X, z, tab):
	printf(" - weight on (0,-infty) in fibrationBasis: %d (%d terms)\n", max(0,seq(`+`(map(nops,w)), w in [indices(tab,'nolist')])), numelems(tab)):
	fibreIntegration(tab, subsop(2=NULL, allvars), resTab):


	printf("line integral 0->infinity:\n"):
	tab := table(sparsereduced):
	axisLimit(Prim, z, tab):
	printf(" - weight on (0,infty) in fibrationBasis: %d (%d terms)\n", max(0,seq(`+`(map(nops,w)), w in [indices(tab,'nolist')])), numelems(tab)):
	fibreIntegration(tab, subsop(2=NULL, allvars), resTab):

	printf("Simplifying dlog's in result\n"):
	return add(combineDlogs(eval(rhs(w),zeta[2]=Zeta(2)))*mul(`if`(lhs(w)[k-2]=[],1,Hlog(allvars[k], lhs(w)[k-2])), k=3..nops(allvars)), w in entries(resTab, 'pairs')):
end proc:

# normalize dlogs such that the coefficient of the first variable in the argument is 1
dlogSimplify := proc(expr)
local S:
option remember:
	S:=indets(expr):
	if S={} then
		return 0:
	else
		return dlog(expr/coeff(expr,S[1],1)):
	end if:
end proc:

wedgeSimplify := proc(forms::list)
local i, L, pre, S, c, f, j:
option remember:
global WEDGE_SIMPLIFY:
	if forms=[] then
		return 1:
	end if:
	# normalize the coefficient of the "first" dlog to 1
	pre := 1:
	L := []:
	for i from 1 to nops(forms) do
		f := eval(forms[i], dlog=dlogSimplify):
		S := selectfun(f, dlog):
		if S={} then
			return 0:
		end if:
		S := S[1]:
		c := coeff(f, S, 1):
		pre := pre*c:
		L:=[op(L), expand(f/c)]:
	end do:
	# Equal forms?
	if nops({op(L)})<nops(L) then
		return 0:
	end if:

	if WEDGE_SIMPLIFY then

	# are there slots with only one form? 
	# If so, can remove this form from all other slots
	for i from 1 to nops(L) do
		S := selectfun(L[i], dlog):
		if nops(S)=1 then
			S := S[1]:
			if has(subsop(i=NULL, L), S) then
				S := wedgeSimplify([seq(eval(L[j], S=0), j=1..i-1), L[i], seq(eval(L[j], S=0), j=i+1..nops(L))]):
#				printf("wedge simplification:\n"):
#				print(wedge(L)=S):
				return pre*S:
			end if:
		end if:
	end do:
	# if there is a form involving only one differential, can remove all forms that only depend on this differential from all other factors:
	for i from 1 to nops(L) do
		S := indets(L[i]):
		if nops(S)>1 then
			next:
		end if:
		c := S[1]:
		S := selectfun(subsop(i=NULL, L), dlog):
		S := select(j->indets(j)={c}, S):
		if S<>{} then
			f := map(j->j=0, S):
			S := wedgeSimplify([seq(eval(L[j], f), j=1..i-1), L[i], seq(eval(L[j], f), j=i+1..nops(L))]):
#			printf("wedge simplification:\n"):
#			print(wedge(L)=S):
			return pre*S:
		end if:
	end do:
	# if the dlog's of a form are a subset of the dlog's of another, we can kill at least one term:
	for i from 1 to nops(L) do
		S := selectfun(L[i], dlog):
		for j from 1 to nops(L) do
			if j=i then
				next:
			end if:
			if not (S subset selectfun(L[j], dlog)) then
				next:
			end if:
			f := S[1]:
			# TODO: choose the form f such that as many dlog's as possible are killed
			S := wedgeSimplify(subsop(j=L[j]-coeff(L[j], f, 1)/coeff(L[i], f, 1)*L[i], L)):
#			printf("wedge simplification:\n"):
#			print(wedge(L)=S):
			return pre*S:
		end do:
	end do:

	end if:

	# sort the forms
	L, c := sort(L, 'output=[sorted,permutation]'):
	pre := pre*GroupTheory[PermParity](Perm(c)):
	return pre*wedge(L):
end proc:

# tries to combine linear combinations of wedge products
# looks for pairs differing in at most one column
combineDlogs := proc(expr)
local S, res, pair, found, A, B, a, b, i, j, L:
	return eval(expr, wedge=wedgeSimplify):
	res := expr:
	while true do
		S := selectfun(res, wedge):
		if nops(S)<=1 then
			break:
		end if:
		found := false:
		for pair in combinat[choose](S,2) do
			A := {op(op(pair[1]))}:
			B := {op(op(pair[2]))}:
			if numelems(A minus B)<>1 then
				next:
			end if:
			if numelems(B minus A)<>1 then
				next:
			end if:
			if numelems(A)<>numelems(B) then
				error "unexpected":
			end if:
			found := true:
			a := op(A minus B):
			b := op(B minus A):
			i := ListTools[Search](a, op(pair[1])):
			j := ListTools[Search](b, op(pair[2])):
			L := op(pair[1]):
			L[i] := expand(
					coeff(res, pair[1], 1)*L[i]
					+(-1)^(i-j)*coeff(res, pair[2], 1)*op(pair[2])[j]
			):
			res := eval(res, [pair[1]=0, pair[2]=0])+wedgeSimplify(L):
			break:
		end do:
		if not found then
			break:
		end if:
	end do:
#	printf("Simplified:\n"):
#	print(expr):
#	printf("to"):
#	print(res):
	return res:
end proc:

dlogsIterRes := proc(forms::list, vars::list)
option remember:
local poles, res, z, i, j, w, L, pole, cs, todo:
global COMBINE_ITERRES:
	if 0 in forms then
		return 0:
	elif vars=[] then
		if forms=[] then
			return 1:
		else
			return wedgeSimplify(forms):
		end if:
	elif forms=[] then
		# the constant 1 does not have any residues
		return 0:
	end if:

	# expand linear combinations
#	for k from 1 to nops(w) do
#		if op(0,w[k])=`+` and false then
#			printf("Expanding sum %a (at %d) in:\n", w[k], k):
#			return add(coeff(w[k],z,1)*dlogsIterRes(subsop(k=z, w), vars), z in selectfun(w[k], ln)):
#		end if:
#	end do:

	res := 0:
	poles := select(w->has(w, vars[1]), selectfun(forms, dlog)):
	for pole in poles do
		# singularity wrt vars[1] at:
		z := op(pole):
		z := -coeff(z, vars[1], 0)/coeff(z,vars[1],1):
		# which of the forms have this residue?
		cs := map(coeff, forms, pole, 1):

		L := eval(forms, pole=0):
		L := eval(L, vars[1]=z):

		todo := select(j->cs[j]<>0, {seq(1..nops(forms))}):
		if todo={} then
			error "dlog(%1) present but with zero coefficient", vars[1]-z:
		end if:
		while (nops(todo)>=2) and COMBINE_ITERRES do
			# special case with precisely two forms: combine wedges by linearity! (one term instead of two)
			i, j := min(todo), max(todo):
			todo := todo minus {i,j}:

#			w := cs[i]*(-1)^(i-1)*wedge(subsop(k=NULL, w))
#			   + cs[j]*(-1)^(j-1)*wedge(subsop(k=NULL, w)):
			w := (-1)^(j-1)*wedgeSimplify(subsop(i=L[i]*cs[j]-L[j]*cs[i], j=NULL, L)):

			w := convert(w, 'IterRes', vars[2..]):
			res := res+den(vars[1], z)*w:

			next:
		end do:
		# general case
		for i in todo do
			w := cs[i]*(-1)^(i-1)*wedgeSimplify(subsop(i=NULL, L)):

			w := convert(w, 'IterRes', vars[2..]):
			res := res+den(vars[1], z)*w:

		end do:
	end do:
	res:
end proc:

`convert/IterRes` := proc(expr, vars::list)
	eval(expr, wedge=(L->dlogsIterRes(L, vars))):
end proc:

# I do not want to remove {z[i]}'s, because they are not taken care of separately in the reduction
irreducibles := proc(S)
	# do not remove variables like z[i], as they should be counted for the best order search!
	# simply normalize the coefficient of the "smallest" variable:
	map(p->`if`(indets(p)={}, NULL, p/coeff(p, indets(p)[1], 1)), S):
end proc:


Reduction := proc(vars::list)
option remember:
local Cs, c, p, H, i, j, zero:
global bubbles:
	Cs := {}:
	for c in Reduction(vars[..-2]) do
		for p in c do
			if degree(p, vars[-1])=0 then
				next:
			end if:
			zero := solve(p, vars[-1]):
			H := {irreducibles(eval(c minus {p}, vars[-1]=zero))}:
			Cs := Cs union H:
		end do:
	end do:
	# remove cliques which are contained in a bigger clique
	p := Cs:
	for c in p do
		Cs := remove(v->(nops(v)<nops(c)) and (v subset c), Cs):
	end do:
	Cs:
end proc:

score := proc(vars::list)
	[seq(nops(select(has, `union`(op(Reduction(vars[..i-1]))), vars[i])), i=1..nops(vars))]:
end proc:

# propagator family according to Rossi&Willwacher
#  - 'harmonic angle' (Kontsevich):    t=1/2 (default)
#  - 'logarithmic propagator':         t=0
# does NOT include the prefactor 1/(2*I*Pi)
Propagator := proc(v, w, t:=1/2)
	(1-t)*(dlog(z[v]- z[w])-dlog(zz[v]- z[w]))
	+   t*(dlog(z[v]-zz[w])-dlog(zz[v]-zz[w]))
end proc:


bestOrder := proc(G::list(list), V2::list)
local S, tau, V, vars, M, Best, i, Poles, top, tops:
	V := map(op, {op(G)}) minus {op(V2)}:
	Poles := map(op, selectfun(map(Propagator@op, G), dlog)):
	if nops(V2)>0 then
		Poles := eval(Poles, [z[V2[1]]=0,zz[V2[1]]=0,seq(op([z[q]=q,zz[q]=q]), q in V2[2..])]):
	end if:
	Poles := irreducibles(Poles):

	forget(Reduction):
	Reduction([]) := {Poles}:

	if nops(V2)<2 then
		Best := {[]}:
	else
		Best := {V2[2..]}:
	end if:

	for i from 1 to nops(V) do
		Best := `union`(seq({seq([op(tau), v], v in (V minus {op(tau)}))}, tau in Best)):
		top := infinity:
		tops := {}:
		for tau in Best do
			printf("order %a:\n", tau):
			vars := [seq(op([z[i],zz[i]]), i in tau)]:
			M := score(vars):
			# turn list of alphabet sizes into one big number: (base = 2*nops(V) = max. alphabet size)
			M := add((2*nops(V))^(nops(vars)-i)*M[i], i=1..nops(vars)):
			if M>top then
				next:
			elif M<top then
				top := M:
				tops := {tau}:
			else
				tops := tops union {tau}:
			end if:
		end do:
		Best := tops:
	end do:
	print(Best):
	Best := Best[1]:
	if nops(V2)>0 then
		return Best[nops(V2)..]:
	else
		return Best:
	end if:
end proc:

frozen := table():
myFreezeHlogs := proc(expr)
local hlogs:
global frozen:
	if expr=0 then
		return 0:
	elif op(0, expr)<>`+` then
		return expr:
	end if:
	hlogs := selectfun(expr, Hlog):
	if hlogs<>{} then
		hlogs := hlogs[1]:
		return add(hlogs^i*myFreezeHlogs(coeff(expr, hlogs, i)), i=0..1):
	end if:
	frozen[numelems(frozen)+1] := expr:
	Frozen[numelems(frozen)]:
end proc:

# hides all wedges
myFreeze := proc(expr, v::posint)
local a, b, res, X, objs, wedgeF, toInd:
global frozen:

	objs := [op(selectfun(expr, wedge))]:
	for a in objs do
		frozen[numelems(frozen)+1] := a:
		toInd[op(a)] := numelems(frozen):
	end do:
	wedgeF := proc(dd)
		Frozen[toInd[dd]]:
	end proc:
	return eval(expr, wedge=wedgeF):

	# old part for den's:
(*
	densZ := select(has, selectfun(expr, den), {z[v],zz[v]}):
	densZ, densZZ := selectremove(has, densZ, z[v]):
	densZZ := select(has, densZZ, zz[v]):

	res := 0:
	for a in densZ do
	for b in densZZ do
		X := coeff(coeff(expr, a, 1), b, 1):
		if X=0 then
			next:
		end if:
		res := res + a*b*myFreezeHlogs(X):
	end do:
	end do:
	res:
*)
end proc:

myThaw := proc(expr)
global frozen:
	eval(expr, Frozen=frozen):
	expand(%):
end proc:

# evaluate the rational function f, defined by
# forms[1] wedge ... wedge forms[k] = f * d(z1) wedge ... wedge d(zk)
# for the arguments zk=atk where at= [[z1,at1],...,[zk,atk]]
# NOTE: exact arithmetic
wedgeEval := proc(forms::list, at::list({symbol,indexed}=numeric))
uses LinearAlgebra:
	Determinant(Matrix([seq([seq(eval(diff(eval(forms[i],dlog=ln), lhs(at[j])), at), j=1..nops(forms))], i=1..nops(forms))])):
end proc:

# evaluate the function f mod p
# assumes that the first nops(forms) entries of at define the volume form differentials
wedgeEvalMod := proc(forms::list, at::list({symbol,indexed}=numeric), modulus::posint)
uses LinearAlgebra[Modular]:
	Determinant(modulus, Mod(modulus, [seq([seq(eval(diff(eval(forms[i],dlog=ln), lhs(at[j])), at), j=1..nops(forms))], i=1..nops(forms))], integer[])):
end proc:

# create a configuration (xi<>xj for i<>j) of N variables, not 0 or 1, drawing values by calling ran()
sampleConfiguration := proc(N::integer, ran::procedure)
local j, got, z:
	got := [0,1]:
	for j from 1 to N do
		z := ran():
		while z in got do
			z := ran():
		end do:
		got := [op(got), z]:
	end do:
	got[3..]:
end proc:

# checks a relation wedge(...) = linear combination of other wedges
# by evaluating at random integer configurations
checkWedgeRelation := proc(expr::equation)
global WEDGE_CHECK_SAMPLES, WEDGE_CHECK_RAN:
global volvars:
local k, X, wedges, vars, sample, test, deg, vvol:
	if not assigned(WEDGE_CHECK_RAN) then
		WEDGE_CHECK_RAN := rand(50):
	end if:
	X := lhs(expr)-rhs(expr):
	wedges := selectfun(X, wedge):
	if nops(wedges)=0 then
		error "no wedges in %1", expr:
	end if:
	deg := nops(op(wedges[1])):
	vars := indets(map(op, selectfun(X, dlog))):
	vvol := select(w->w in vars, volvars):
	if nops(vvol)<deg then
		# in this case, expr should be wedge()=0
		if expr<>(wedges[1]=0) then
			error "wedge with too few variables; simple wedge=0 expected, but got %1", expr:
		end if:
		return:
	elif nops(vvol)>deg then
		# This cannot ever happen!
		error "too many variables: %1 in %2 while volvars=%3", vvol, expr, allvars:
	end if:
	vars := [op(vvol), op(vars minus {op(vvol)})]:
	for k from 1 to WEDGE_CHECK_SAMPLES do
		sample := sampleConfiguration(nops(vars), WEDGE_CHECK_RAN):
		sample := [seq(vars[i]=sample[i], i=1..nops(vars))]:
		test := add(coeff(X, w, 1)*wedgeEval(op(w), sample), w in wedges):
		test := simplify(test):
		if test<>0 then
			printf("Testing wedge relation:\n"):
			print(expr):
			printf("at the point:\n"):
			print(sample):
			printf("LHS-RHS = %a\n", test):
			error "wedge relation failed!":
		end if:
	end do:
end proc:

# choose a basis among the wedges and express all other wedge's in terms of this basis
# uses rational arithmetic and dense matrices for the reduction
rationalBasisQ := proc(expr, volvars::list)
global allvars:
local modulus, samples, Nsamples, vars, i, ran, j, s, got, z, M, B, dim, MM, SUBSTI, RHS, SOL, V, iDens:

	V := [op(selectfun(expr, wedge))]:
	if V=[] then
		return expr:
	end if:
	printf("wedge's in input: %d\n", nops(V)):

	# sort V's by complexity (want to choose basis among the simplest):
	V := sort(V, length):

	if not ({op(map(nops@op, V))} subset {nops(volvars)}) then
		error "wrong weight: expected %1=nops(%2); but got forms %3", nops(volvars), volvars, V:
	end if:

	vars := indets(map(op, V)) intersect {op(allvars)}:
	vars := vars minus {op(volvars)}:
	printf("Extra variables: %a\n", vars):
	vars := [op(volvars), op(vars)]:

	# extract the denominators appearing anywhere:
	iDens := lcm(op(map(denom, map(op@op, V)))):
	printf("LCM of denominators: %d\n", iDens):

	# all variables must get different values, and different from 0 and 1: finite field needs to have at least 2+nops(vars) elements
	# + some extra leeway
	modulus := nextprime(nops(vars)+2+20):
	# make sure all rational coefficients are defined:
	while (iDens mod modulus) = 0 do
		modulus := nextprime(modulus):
	end do:

	# points for evaluation:
	Nsamples := nops(V)+max(5, floor(nops(V)*0.2)):
	ran := rand(modulus):
	samples := {}:
	while numelems(samples)< Nsamples do
		samples := samples union {sampleConfiguration(nops(vars), ran)}:
	end do:
	printf("Generated %d samples modulo %d of %a:\n", Nsamples, modulus, vars):

	# columns are the forms, rows the sample points
	M := LinearAlgebra[Modular][Mod](modulus, [seq([seq(wedgeEvalMod(op(V[i]), [seq(vars[k]=samples[j][k], k=1..nops(vars))], modulus), i=1..nops(V))], j=1..Nsamples)], integer[]):
	print(M):

	# choose a basis among the simplest elements (greedy):
	B := LinearAlgebra[Modular][RankProfile](modulus, M):
	dim := nops(B):
	printf("Rank: %d\n", dim):
	printf("Selected basis: %a\n", B):
	if dim=0 then
		for i in V do
			checkWedgeRelation(i=0):
		end do:
		return eval(expr, wedge=0):
	end if:

	# Select dim=nops(B) independent sample points:
	MM := LinearAlgebra[Modular][Transpose](modulus, M):
	got := LinearAlgebra[Modular][RankProfile](modulus, MM):
	if nops(got)<>dim then
		error "samples rank = %1 <> wedges rank %2", nops(got), dim:
	end if:
	samples := [seq(samples[i], i in got)]:
	printf("Selected independent samples:\n"):
	print(samples):
	
	# Express every element in the chosen basis
	# Now work with rational coefficients!

	M := Matrix([seq([seq(wedgeEval(op(V[B[i]]), [seq(vars[k]=samples[j][k], k=1..nops(vars))]), i=1..dim)], j=1..dim)]):
	printf("Independent sample matrix:\n"):
	print(M):

	MM := M^(-1):
	printf("Inverse matrix:\n"):
	print(MM):

	SUBSTI := []:
	for i from 1 to nops(V) do
		if i in B then
			next:
		end if:
		RHS := Vector([seq(wedgeEval(op(V[i]), [seq(vars[k]=samples[j][k], k=1..nops(vars))]), j=1..dim)]):
		SOL := MM.RHS:
		printf("Solution for %d:\n", i):
		lprint(convert(SOL, 'list')):
		SUBSTI := [op(SUBSTI), V[i] = add(SOL[j]*V[B[j]], j=1..dim)]:
		checkWedgeRelation(SUBSTI[-1]):
	end do:
	return eval(expr, SUBSTI):
end proc:

# sample iterated residue
# only produces residues that a function with denominators in "Poles" can possibly have
# uniform among those (in some sense; see TODO below)
sampleResidue := proc(Poles::set, vars::set)
local at, var, pole:
	if not (vars subset `union`(op(Poles))) then
		return false:
	end if:
	if nops(Poles)<nops(vars) then
		return false:
	end if:
	if vars={} then
		return []:
	end if:

	# pick a divisor at random
	pole := Poles[1+(rand() mod nops(Poles))]:
	# pick a variable at random
	var := vars intersect pole:
	if var={} then
		error "constant divisor: %1 independent of %2", pole, vars:
	end if:
	if nops(var)=1 then
		var := var[1]:
	else
		var := var[1+(rand() mod 2)]:
	end if:
	at := op(pole minus {var}):
	# TODO: first picking variable and then picking divisor would give a different sample distribution! Which is better?

	# Polynomial reduction (after removing constants):
	pole := sampleResidue(select(has, eval(Poles minus {pole}, var=at), vars), vars minus {var}):
	if type(pole, 'list') then
		return [var=at, op(pole)]:
	else
		return false:
	end if:
end proc:

# compute iterated residues of a wedge product of dlog's
# expects that dlog's are written as dlog({a,b})
# returns an integer
wedgeRes := proc(forms::list, poles::list)
local L, M, i, j, var, at:
option remember:
	L := forms:
	M := Matrix(1..nops(forms), 1..nops(poles)):
	for i from 1 to nops(poles) do
		var, at := op(poles[i]):
		for j from 1 to nops(forms) do
			M[i,j] := coeff(L[j], dlog({var,at}), 1):
		end do:
		L := eval(L, var=at):
	end do:
	M := LinearAlgebra[Determinant](M):
	return M:
end proc:

# choose a basis among the wedges
# reduction via residue functionals
rationalBasis := proc(expr, volvars::list)
local modulus, samples, Nsamples, i, ran, j, s, got, z, M, B, dim, MM, SUBSTI, RHS, SOL, V, Poles, BasisForms, todo, kk, allzero, iDens:
local vars:
uses LinearAlgebra:
global allvars:

	V := [op(selectfun(expr, wedge))]:
	if V=[] then
		return expr:
	end if:
	printf("wedge's in input: %d\n", nops(V)):

	# Faster for few wedges:
	if nops(V)<50 then
		return rationalBasisQ(expr, volvars):
	end if:

	# sort V's by complexity (want to choose basis among the simplest):
	V := sort(V, length):

	vars := indets(map(op, V)) intersect {op(allvars)}:
	vars := vars minus {op(volvars)}:
	printf("Extra variables: %a\n", vars):
	vars := [op(volvars), op(vars)]:

	# extract the denominators appearing anywhere:
	iDens := lcm(op(map(denom, map(op@op, V)))):
	printf("LCM of denominators: %d\n", iDens):

	# all variables must get different values, and different from 0 and 1: finite field needs to have at least 2+nops(vars) elements
	# + some extra leeway
	modulus := nextprime(nops(vars)+2+20):
	# make sure all rational coefficients are defined:
	while (iDens mod modulus) = 0 do
		modulus := nextprime(modulus):
	end do:

	# points for evaluation:
	Nsamples := nops(V)+max(5, floor(nops(V)*0.2)):
	ran := rand(modulus):
	samples := {}:
	while numelems(samples)< Nsamples do
		samples := samples union {sampleConfiguration(nops(vars), ran)}:
	end do:
	printf("Generated %d samples modulo %d of %a:\n", Nsamples, modulus, vars):

	# columns are the forms, rows the sample points
	M := LinearAlgebra[Modular][Mod](modulus, [seq([seq(wedgeEvalMod(op(V[i]), [seq(vars[k]=samples[j][k], k=1..nops(vars))], modulus), i=1..nops(V))], j=1..Nsamples)], integer[]):
	print(M):

	# choose a basis among the simplest elements (greedy):
	B := LinearAlgebra[Modular][RankProfile](modulus, M):
	dim := nops(B):
	printf("Rank: %d\n", dim):
	printf("Selected basis: %a\n", B):
	if dim=0 then
		for i in V do
			checkWedgeRelation(i=0):
		end do:
		return eval(expr, wedge=0):
	end if:

	# all singularities in the input: dlog(a-b) -> {a,b}
	# note that we can restrict to the residues from the basis elements; all other residues must vanish
	Poles := map(op, selectfun([seq(V[i], i in B)], dlog)):
	Poles := {seq(seq({i, solve(j,i)}, j in select(has, Poles, i)), i in volvars)}:
	printf("Poles of basis functions:\n"):
	print(Poles):

	# rewrite dlog(a-b) as dlog({a,b}):
	BasisForms := eval(V, dlog=(foo->dlog({indets(foo)[1], solve(foo, indets(foo)[1])}))):
	printf("replaced dlog(a-b) by dlog({a,b})\n"):

	# sample linearly independent iterated residues:
	samples := []:
	s := table():
	while nops(samples)<dim do
		i := dim+10:
		j := 0:
		got := 0:
		while got<i do
			j := j+1:
			z := sampleResidue(Poles, {op(volvars)}):
			if not type(z, 'list') then
				next:
			end if:
			# filter out zeros:
			allzero := true:
			for kk from 1 to dim do
				if wedgeRes(op(BasisForms[B[kk]]), z)<>0 then
					allzero := false:
					break:
				end if:
			end do:
			if allzero then
				next:
			end if:
			got := got+1:
			s[got] := z:
		end do:
		printf("needed %d samples to generate %d residues\n", j, got):
		samples := [op(samples), seq(s[i], i=1..got)]:

		# evaluate residues on basis forms:
		M := Matrix([seq([seq(wedgeRes(op(BasisForms[B[i]]), samples[j]), j=1..nops(samples))], i=1..dim)]):
		printf("residue sample matrix:\n"):
		print(M):

		# choose linearly independent residues:
#		got := LinearAlgebra[RankProfile](M):
#		samples := [seq(samples[i], i in got)]:
		M := LinearAlgebra[GaussianElimination](M):
		# find pivots:
		got := []:
		# next point to look at:
		i := 1: # row
		j := 1: # col
		while (i<=dim) and (j<=nops(samples)) do
			if M[i,j]<>0 then
				got := [op(got), j]:
				i := i+1:
			end if:
			j := j+1:
		end do:
		samples := [seq(samples[i], i in got)]:

		printf("Independent residues: %d\n", nops(samples)):
		
	end do:

	printf("Selected independent samples:\n"):
	print(samples):

	# evaluate residues on basis forms:
	M := Matrix([seq([seq(wedgeRes(op(BasisForms[B[i]]), samples[j]), j=1..nops(samples))], i=1..dim)]):
	M := LinearAlgebra[Transpose](M):
	printf("residue sample matrix:\n"):
	print(M):

	MM := M^(-1):
	printf("Inverse matrix:\n"):
	print(MM):

	SUBSTI := []:
	for i from 1 to nops(V) do
		if i in B then
			next:
		end if:
		RHS := Vector([seq(wedgeRes(op(BasisForms[i]), samples[j]), j=1..dim)]):
		SOL := MM.RHS:
		printf("Solution for %d:\n", i):
		lprint(convert(SOL, 'list')):
		SUBSTI := [op(SUBSTI), V[i] = add(SOL[j]*V[B[j]], j=1..dim)]:
		checkWedgeRelation(SUBSTI[-1]):
	end do:

	return eval(expr, SUBSTI):
end proc:

# takes resides wrt vars and puts them into tab
dlogsIterResToTable := proc(forms::list, vars::list, tab::table, word::list:=[], pre:=1)
local poles, res, z, i, j, w, L, pole, cs, todo:
global COMBINE_ITERRES:
	if 0 in forms then
		return:
	elif vars=[] then
		if forms=[] then
			tab[word] := tab[word] + pre:
		else
			tab[word] := tab[word] + pre*wedgeSimplify(forms):
		end if:
		return:
	elif forms=[] then
		# the constant 1 does not have any residues
		return:
	end if:

	poles := select(w->has(w, vars[1]), selectfun(forms, dlog)):
	for pole in poles do
		# singularity wrt vars[1] at:
		z := op(pole):
		z := -coeff(z, vars[1], 0)/coeff(z,vars[1],1):
		# which of the forms have this residue?
		cs := map(coeff, forms, pole, 1):

		L := eval(forms, pole=0):
		L := eval(L, vars[1]=z):

		todo := select(j->cs[j]<>0, {seq(1..nops(forms))}):
		if todo={} then
			error "dlog(%1) present but with zero coefficient", vars[1]-z:
		end if:
		while (nops(todo)>=2) and COMBINE_ITERRES do
			# special case with precisely two forms: combine wedges by linearity! (one term instead of two)
			i, j := min(todo), max(todo):
			todo := todo minus {i,j}:

#			w := cs[i]*(-1)^(i-1)*wedge(subsop(k=NULL, w))
#			   + cs[j]*(-1)^(j-1)*wedge(subsop(k=NULL, w)):

			dlogsIterResToTable(subsop(i=L[i]*cs[j]-L[j]*cs[i], j=NULL, L), vars[2..], tab, [z, op(word)], (-1)^(j-1)*pre):
		end do:
		# general case
		for i in todo do
			dlogsIterResToTable(subsop(i=NULL, L), vars[2..], tab, [z, op(word)], pre*cs[i]*(-1)^(i-1)):
		end do:
	end do:
end proc:

TerrestrialIntegrals := proc(expr, vars::list)
global allvars, volvars:
local res, T, k, S, X, w, SUBS:
	printf("Terrestrial integrations: %a\n", vars):
	# read into table:
	T := table(sparsereduced):
	for S in selectfun(expr, wedge) do
		dlogsIterResToTable(op(S), vars, T, [], coeff(expr, S, 1)):
	end do:

	# simplify wedges:
	printf("Simplifying wedges...\n"):
	X := selectfun([entries(T,nolist)], wedge):
	X := [op(X)]:
	print(X):
	S := rationalBasis(X, volvars):
	print(S):
	if not type(S, list) then
		error "list expected from rationalBasis, but got %1", S:
	end if:
	if nops(S)<>nops(X) then
		error "Wrong number of elements; got %1 instead of %2 in %3", nops(S), nops(X), S:
	end if:
	SUBS := [seq(`if`(X[k]<>S[k], X[k]=S[k], NULL), k=1..nops(X))]:
#	SUBS := []:
	for w in indices(T,nolist) do
		T[w] := eval(T[w], SUBS):
	end do:
printf("\nSubstitutions:\n"):
print(SUBS):
printf("\n\n"):
	# fibrationBasis:
	printf("fibrationBasis...\n"):
	X := [seq([rhs(w), [lhs(w)]], w in [indices(T,pairs)])]:
	X := fibrationBasis(X, allvars):
	return X:
end proc:

# G = [[e11,e12,...],[e21,e22,...],...,[en1,...]] is an adjacency list
# [e11,e12,...] are the out-neighbours of vertex 1 and so on
weight := proc(G::list(list), tProp::numeric := 1/2)
local VR, M, J, e, v, pre, Props, n, m, E:
global allvars, delta:
global volvars: # the variables that make up the volume form

	# reset anything hyperInt has remembered
	forgetAll():

	# check graph
	n := nops(G):
	E := [seq(seq([i, G[i][j]], j=1..nops(G[i])), i=1..n)]:
	m := nops(E)+2-2*n:
	printf("Graph of type (n,m)=(%d,%d)\n", n, m):

	# special cases without propagators:
	if [n,m] in {[1,0],[0,2]} then
		return 1:
	end if:

	# vertices of type 2 (on the real line):
	VR := {seq(op(e), e in G)} minus {seq(1..n)}:
	if not (VR subset {'L', 'R', seq(cat(p,i), i=1..m)}) then
		error "terrestrial vertices %1 should be labelled L, R, or p1, ..., p%2", VR, m:
	end if:
	E := eval(E, ['L'=p1, 'R'=cat(p,m)]):
	VR := [cat(p, 1..m)]:


	# find best order:
	# for the wheels, this is much better:
	if (m=0) and (n>4) then
		allvars := [seq(1..n)]:
	else
		allvars := bestOrder(E, VR):
	end if:

	printf("Integration order: %a\n", allvars):

	# Compute integrand:
	pre := 1/(2*Pi*I)^(2*n+m-2): # prefactor in the definition of the weight
	Props := eval(
		[seq(Propagator(e[1], e[2], tProp), e in E)],
		[seq(op([z[q]=q,zz[q]=q]), q in VR)]
	):
	Props := eval(Props, p1=0):

	allvars := [seq(op([z[v],zz[v]]), v in allvars)]:
	if m>0 then
		volvars := subsop(-2=NULL, allvars):
	else
		volvars := allvars[..-3]:
	end if:
	for v from 1 to n do
		delta[z[v]] := 1:
		delta[zz[v]] := -delta[z[v]]:
		# for transformation to infinity:
		delta[1/z[v]] := -delta[z[v]]:
		delta[1/zz[v]] := -delta[zz[v]]:
	end do:
	# Integrand as a symbolic wedge of the angle differentials:
	M := wedgeSimplify(Props):

	# Integrate out V2 vertices:
	if m>1 then
		M := TerrestrialIntegrals(M, VR[2..]):
	end if:



	for v from 1 to n do
		printf("Integration #%d: vertex %d\n", v, op(allvars[1])):

		# make residues in z[v] and zz[v] explicit:
		if nops(volvars)>4 then # only for the next integration:
			M := convert(M, 'IterRes', volvars[1..2]):
		else # full expansion:
			M := convert(M, 'IterRes', volvars):
		end if:

		if v=n then
			# If m=0, the remaining function should already be independent of the last variable
			if m=0 then
				break:
			end if:

			# Only compute a half-residue at the end:
			M := (-1)^(m-1)*2*M:

			resTab := table(sparsereduced):
			tab := table(sparsereduced):
			HlogsIntoTable(M, allvars, tab):
			lastInt(tab, resTab):


			M := add(eval(rhs(w),zeta[2]=Zeta(2))*mul(`if`(lhs(w)[k-2]=[],1,Hlog(allvars[k], lhs(w)[k-2])), k=3..nops(allvars)), w in entries(resTab, 'pairs')):

			printf("\n\nRESULT:\n\n"):
			lprint(M):
			break:

		end if:

		# choose a basis in the space of rational functions in allvars[3..]
		M := rationalBasis(M, volvars[3..]):
		BEFORE[v] := M:

		M := myFreeze(M, op(allvars[1])):
		M := svInt(M/(I*Pi*2)):
		pre := pre*(I*Pi*2):
		M := myThaw(M):

		printf("\n\nRESULT:\n\n"):
		lprint(M):
		AFTER[v] := M:
		save AFTER, BEFORE, "temp.m":

		allvars := allvars[3..]:
		volvars := volvars[3..]:
	end do:
	M := simplify(M*pre):
	printf("Final result:\n"):
	print(M):
	lprint(M):
	return M:
end proc:

read "graphs/graphs.mpl":
getGraph := proc(hbar::posint, id::posint)
local f, name, di6, i:
	name := cat("graphs/kg", hbar):
	if not FileTools[Exists](name) then
		error "File %1 not found", name:
	end if:
	if id>FileTools[Text][CountLines](name) then
		error "undefined graph %1; there are only %2 at order %3", id, FileTools[Text][CountLines](name), hbar:
	end if:
	for i from 1 to id do
		di6 := FileTools[Text][ReadLine](name):
	end do:
	FileTools[Text][Close](name):
	parseDigraph6(di6):
end proc:

if assigned(hbar) and assigned(g) then
	# read graph from nauty's list:
	G := getGraph(hbar, g):
	print(G):
	# compute and store weight
	if not assigned(t) then
		t := 1/2:
	end if:
	w := weight(G, t):
	save G, w, t, cat("weights/order", hbar, "graph", g):
end if:
