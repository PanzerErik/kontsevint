# Erik Panzer
# started 12 July 2016

# Code to evaluate the weights in Kontsevich's formula

# sums over all orders of imaginary parts
# for each of those, integrates real parts (taking residues)
# separate integration of the largest imaginary part (exploit scaling)

interface(errorbreak=2):
read "HyperInt.mpl":
_hyper_check_divergences := false:

# Only computes the iterated residues, i.e. assumes that all higher poles (and polynomial parts) cancel
iterRes := proc(expr, vars::list)
local res, S, i, p, X, Z, N, P, Q, f, zero, s:
	if op(0,expr)=`+` then
		return map(iterRes, expr, vars):
	end if:
	if vars=[] then
		return normal(expr):
	end if:

	P := normal(expr):
	if P=0 then
		return 0:
	end if:
	P, Q := numer(P), denom(P):
	# Find zeros of denominator
	Q, Z := op(factors(Q)):
	Z, S := selectremove(has, Z, vars[1]):
	Q := Q*mul(f[1]^f[2], f in S):
	res := 0:
	for zero in Z do
		X := P/Q/mul(`if`(f=zero, diff(f[1], vars[1]), f[1])^f[2], f in Z):
		s := -coeff(zero[1], vars[1], 0)/coeff(zero[1], vars[1], 1):
		if zero[2]=1 then
			X := eval(X, vars[1]=s):
		else
			print(X,factor(expr), vars, s, Z, P, Q, numer(normal(expr)), denom(normal(expr))):
			error "higher order pole":
		end if:
		res := res + den(vars[1],s)*iterRes(X, vars[2..]):
	end do:
	return res:
end proc:

xSIG := proc(expr)
local ImPart, ImSign, S, v:
global imPerm:
	# extract imaginary part and its sign:
	ImPart := coeff(expand(expr), I, 1):
	ImSign := signum(eval(ImPart, [seq(cat(t,v)=1/2, v=1..20),seq(y[imPerm[v]]=v, v=1..nops(imPerm))])):
	return ImSign:
end proc:

oneSIG := proc(expr)
local ImPart, ImSign, S, v:
global imPerm:
	# extract at imaginary part and its sign:
	ImPart := -coeff(expand(expr), I, 1):
	for v from nops(imPerm) to 1 by -1 do
		S := coeff(ImPart, y[imPerm[v]], 1):
		if S<>0 then
			ImSign := signum(S):
			break:
		end if:
	end do:
	return ImSign/2*I*Pi + ln(ImPart*ImSign/2):
	if type(nops(imPerm), odd) then
		return ImSign/2:
	else
		# The factor 1/2 simplifies the argument (which always seem to have coefficients 2)
		# We can take this out here like this because the function must be homogeneous, i.e. we may add an arbitrary constant to all logarithms
		return ln(ImPart*ImSign/2):
	end if:
end proc:

# normalize coefficient of next variable
NORMDEN := proc(expr)
local var, c:
	var := indets(expr):
	if ONE in var then
		var := ONE:
	else
		var := var[1]:
	end if:
	c   := coeff(expr, var, 1):
	1/c*DEN(expand(expr/c)):
end proc:

residues := proc(expr, var)
local Dens, Pre, Z, zero, z, i, j:
	if op(0,expr)=`+` then
		return map(residues, expr, var):
	end if:
	if expr=0 then
		return 0:
	end if:
	if op(0, expr)<>`*` then
		print(expr):
		error "product of DEN's expected":
	end if:
	Dens, Pre := selectremove(w->op(0,w)=DEN, [op(expr)]):
	Z, Dens   := selectremove(has, Dens, var):
	Pre := `*`(op(Pre),op(Dens)):

	# normalize coefficients of var
	Z := map(op, Z):
	for i from 1 to nops(Z) do
		z := coeff(Z[i], var, 1):
		Pre := Pre/z:
		Z[i] := -coeff(Z[i], var, 0)/z:
	end do:

	# find zeros
	if var=ONE then
		add(oneSIG(Z[i])*Pre*(-NORMDEN(Z[i]))*mul(`if`(j=i, 1, NORMDEN(Z[i]-Z[j])), j=1..nops(Z)), i=1..nops(Z)):
	else
		add(xSIG(Z[i])*Pre*mul(`if`(j=i, 1, NORMDEN(Z[i]-Z[j])), j=1..nops(Z)), i=1..nops(Z)):
	end if:
end proc:

weight := proc(G::list(list))
local V, F, M, J, e, v, vars, res, num, ff, pre, angle:
global imPerm, Integrands:

	V := max(eval(map(op,G),[L=0,R=0])):
	if 2*V<>nops(G) then
		error "number of edges is not 2*number of internal vertices":
	end if:

	# harmonic angle:
	angle := (v,w)->log(z[v]-z[w])+log(z[v]-zz[w])-log(zz[v]-z[w])-log(zz[v]-zz[w]):
	# other angle:
#	angle := (v,w)->log(z[v]-z[w])-log(zz[v]-z[w]):

	# propagators:
	F := [seq(angle(e[1], e[2]), e in G)]:
	F := eval(F, [z[L]=0,zz[L]=0,z[R]=ONE,zz[R]=ONE,seq(z[v]=x[v]+I*y[v], v=1..V),seq(zz[v]=x[v]-I*y[v],v=1..V)]):
	vars := [seq(op([x[v],y[v]]), v=1..V)]:

	F := eval(F, ln=(a->ln(a+dummy))):
	M := [seq([seq(iterRes(diff(e,v), [dummy]), v in vars)], e in F)]:
	M := eval(M, den=((a,b)->DEN(-b))):
	J := LinearAlgebra[Determinant](M):

	pre := 1/V!/(2*Pi)^(2*V)/(2*I)^(2*V):

	# Check determinant:
(*
	M := [seq([seq(diff(eval(e, dummy=0),v), v in vars)], e in F)]:
	M := LinearAlgebra[Determinant](M):
	if simplify(M-eval(J, DEN=((a)->1/(a))))<>0 then
		print(SOLL=factor(M)):
		print(IST=factor(eval(J, DEN=((a)->1/(a))))):
		error "Messed up determinant":
	end if:
*)
	print(J):

	# Consider all configurations (total orders) separately
	pre := pre*(I*Pi)^V: # this is for the residues
	# for odd V, take out another pi, such that pre and the integrand are always real:
	# Actually, do not do that, because we also want to consider the other angle function!
#	if type(V, odd) then
#		pre := pre*(I*Pi):
#		J   := J/(I*Pi):   # the I*Pi is removed in SIG()
#	end if:
	res := 0:
	num := 0:
	for imPerm in combinat[permute]([seq(1..V)]) do
		printf("Permutation: %a\n", imPerm):

		M := J:
		printf(" - integrand: %d terms\n", nops(M)):

		# Iterative residue theorem for x-integrations
		for v from 1 to V do
			printf(" - residues wrt x[%d]: ", v):
			M := residues(M, x[v]):
			printf("%d terms\n", nops(M)):
		end do:

		# Integrate over scale variable
		printf(" - integrating scale variable: "):
		M := residues(M, ONE):
		printf("%d terms\n", nops(M)):

		M := eval(M, y[imPerm[1]]=1):
		M := eval(M, DEN=(a->1/a)):
		ff := selectfun(M, ln) union {Pi}:
		M := add(v*expand(iterRes(coeff(M, v, 1), [seq(y[imPerm[V+1-u]], u=1..V-1)])), v in ff):
		printf(" - after collecting logs: %d terms\n", nops(M)):
		M := eval(M, den=((a,b)->1/(a-b))):
		print((M)):
		Integrands[imPerm] := M:

(*
		# numeric integration
		printf(" - numeric integration: "):
		# Change variables to integrate over a hypercube
		ff := eval(M, [seq(y[imPerm[v]]=mul(cat(t,k), k=v..V-1), v=1..V-1)]):
		if type(V, 'odd') then
			ff := factor(ff*mul(cat(t,v)^(v-1), v=2..V-1)):
		else
			ff := map(w->factor(w*mul(cat(t,v)^(v-1), v=2..V-1)), ff):
		end if:
		ff := Int(ff, [seq(cat(t,v)=0..1, v=1..V-1)]):
		print(ff):
		ff := evalf[6](ff):
		printf("%.10f\n", ff):
		num := num+ff:
*)

		printf(" - symbolic integration:"):

		M := expand(M):
		if op(0,M)=`+` then
			M := map(f->op(convert(f, HlogRegInf)), [op(M)]):
		end if:
		for v from V to 2 by -1 do
			M := eval(M, y[imPerm[v]]=y[imPerm[v]] + `if`(v=2, 1, y[imPerm[v-1]])):
		end do:
		M := Int(M, [seq(y[imPerm[V+1-v]], v=1..V-1)]):
		M := fibrationBasis(eval(M, Int=hyperInt)):
		M := eval(fibrationBasis(M), zeta[2]=Zeta(2)):
		M := simplify(M):

		printf(" => %d terms\n", nops(M)):
		res := res + M:
	end do:
#	print(evalf(evalf(res))):
	lprint(pre*res):
	simplify(pre*res):
end proc:

`evalf/Hlog` := proc(z, w::list)
	if w=[] then
		return 1:
	elif nops(w)=1 then
		return log(1-z/w[1]):
	elif nops(w)=2 then
		return Int(1/(x-w[1])*log(1-x/w[2]),x=0..z):
	elif nops(w)=3 then
		return Int(1/(x-w[2])*log(1-x/w[3])*log((z-w[1])/(x-w[1])),x=0..z):
	elif nops(w)=4 then
		return Int(1/(x2-w[2])/(x3-w[3])*log(1-x3/w[4])*log((z-w[1])/(x2-w[1])),[x2=x3..z, x3=0..z]):

	else
		error "not implemented":
	end if:
end proc:

read "graphs/graphs.mpl":
getGraph := proc(hbar::posint, id::posint)
local f, name, di6, i:
	name := cat("graphs/kg", hbar):
	if not FileTools[Exists](name) then
		error "File %1 not found", name:
	end if:
	if id>FileTools[Text][CountLines](name) then
		error "undefined graph %1; there are only %2 at order %3", id, FileTools[Text][CountLines](name), hbar:
	end if:
	for i from 1 to id do
		di6 := FileTools[Text][ReadLine](name):
	end do:
	FileTools[Text][Close](name):
	parseDigraph6(di6):
end proc:

if assigned(hbar) and assigned(g) then
	# read graph from nauty's list:
	G := getGraph(hbar, g):
	print(G):
	# compute and store weight
	w := weight(G):
	save G, w, cat("weights/order", hbar, "graph", g):
end if:
