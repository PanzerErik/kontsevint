# Erik Panzer
# Test cases for the computation of Kontsevich weights
interface(errorbreak=2):

#read "kontsv.mpl":
read "kontsv01.mpl":

# Results for the harmonic propagator:
Tests := [
 [[L,R]]=1/2
,[[L,R],[L,R]]=1/4
,[[2,R],[L,R]]=1/12
# result of integrating out z[1]:
#SOLL := 1/16*(z[2]-zz[2])*(I*Hlog(zz[2],[1])-I*Hlog(z[2],[1])+Pi)/(z[2]-1)/(zz[2]-1)/Pi^3/z[2]/zz[2];

,[[2,R],[L,R],[1,R]] = 0
,[[2,R],[L,R],[2,R]] = 1/24
,[[2,L],[L,R],[2,R]] = 0
,[[2,3],[L,R],[2,1]] = 0
,[[2,R],[L,R],[1,L]] = -1/48
,[[2,L],[3,R],[1,L]] = 0
,[[L,R],[L,R],[1,2]] = 0
,[[L,2],[1,R],[1,2]] = 1/48

,[[L,R],[L,R],[L,R],[L,R]] = 1/16
,[[L,2],[R,3],[L,4],[L,1]] = 1/1440
# bernoulli(4)/24^2:
,[[L,R],[1,R],[2,R],[3,R]] =-1/720
,[[R,3],[R,1],[L,2],[1,3]] = 1/11520  #15

,[[2,3],[3,4],[2,5],[5,L],[4,R]] = 1/2880
,[[2,L],[3,R],[4,L],[5,R],[1,L]] = -1/15360
,[[L,R],[L,R],[L,R],[L,R],[L,R]] = 1/32
,[[L,R],[1,R],[2,R],[3,R],[4,R]] = 0
,[[L,2],[R,3],[R,4],[R,5],[L,R]] = 1/2880
,[[L,2],[L,3],[R,4],[R,5],[L,R]] = 1/23040
,[[L,2],[L,3],[L,4],[R,5],[L,R]] = 1/2880
,[[R,2],[L,3],[L,4],[R,5],[L,R]] = 19/46080
,[[R,2],[R,3],[L,4],[R,5],[L,R]] = 0
,[[R,2],[L,3],[R,4],[R,5],[L,R]] = 0
,[[L,2],[R,3],[L,4],[R,5],[L,R]] = 43/46080 #26
# Peter's problematic case:
,[[4,5],[R,5],[R,2],[L,3],[R,4]] = 1/3840  #27
# Brent's problematic case:
,[[3,4],[3,5],[4,5],[1,2],[L,R]] = 0 #28

# simpler (order 6) graph that already has zeta[3]^2:
,[[L,2],[L,3],[L,4],[R,5],[R,6],[L,R]] = 1/2903040*(53*Pi^6-68040*zeta[3]^2)/Pi^6  #29

# Felder & Willwacher:
,[[L,2],[L,3],[L,4],[L,5],[R,6],[R,7],[L,R]] = 1/2903040/Pi^6*(13*Pi^6-11340*zeta[3]^2) #30


# graph 108 @ hbar^5: zero integrand
,[[3,5],[4,5],[1,5],[L,R],[1,3]] = 0 #31

# graph 202 @ hbar^5: 
# terms after parfrac = 17920
,[[3,4],[4,5],[1,5],[L,3],[1,R]]=-1/7680 #32

# graph 404 @ hbar^5:
# Terms after parfrac: 25408
,[[4,5],[4,5],[1,2],[1,L],[R,3]]=-1/23040 #33

# graph 432 @ hbar^5:
# Terms after parfrac: 8064
,[[3,4],[4,5],[1,2],[L,5],[1,R]]=1/46080 #34

# graph 20335 @ hbar^6:
# imaginary part for harmonic weight with old code (that only took monodromies on the first sheet of the naive primitive into account):
,[[2,4],[1,5],[6,4],[L,5],[L,6],[L,R]]=1/774144 #35

# 5-vertex graph with imaginary part (send 4->R in #35, then rename 5->4 and 6->5):
,[[3,4],[R,4],[1,5],[L,5],[L,R]]=1/11520 #36

# graph 8058 @ hbar^5:
# has 1/(z[5]-zz[5]) in rational prefactor in parfrac basis for order [2,3,6,1,4,5]
,[[R,4],[5,6],[5,6],[L,5],[1,2],[L,R]]=-5/129024+3/128*zeta[3]^2/Pi^6 #37

# graph 32937 @ hbar^6:
# has z-zz for integration order [5,3,4,6,1,2]
,[[3,5],[4,6],[L,6],[L,3],[2,R],[1,4]]=-1/92897280*(3131*Pi^6-2114910*zeta[3]^2)/Pi^6 #38

# hbar^8 example with zeta(3,5):
,[[L,2],[R,3],[L,4],[R,5],[L,R],[R,1],[L,6],[R,7]]
= 22427/1990656000-355/49152/Pi^6*zeta[3]^2+65/1024/Pi^8*zeta[3]*zeta[5]+417/10240/Pi^8*zeta[3,5]

# hbar^10 example with zeta(3,7):
,[[L,2],[R,3],[L,4],[R,5],[L,R],[R,1],[L,6],[R,7],[L,8],[R,9]]
= 4483691/68669669376000-45791/11796480/Pi^6*zeta[3]^2+14687/196608/Pi^8*zeta[3]*zeta[5]+25717/1966080/Pi^8*zeta[3,5]-77357/262144/Pi^10*zeta[3]*zeta[7]-433371/7340032/Pi^10*zeta[5]^2-259747/7340032/Pi^10*zeta[3,7]

]:

# Some efficiently computable graphs at order 8:

# [[L,R],[R,1],[R,2],[R,3],[R,4],[R,5],[R,6],[R,7]]=1/1209600
# [[L,R],[R,1],[L,2],[R,3],[R,4],[R,5],[R,6],[R,7]]=13/7257600
# [[L,R],[R,1],[R,2],[L,3],[R,4],[R,5],[R,6],[R,7]]=1/2419200
# [[L,R],[R,1],[R,2],[R,3],[L,4],[R,5],[R,6],[R,7]]=1/725760
# [[L,R],[R,1],[R,2],[R,3],[R,4],[L,5],[R,6],[R,7]]=1/2419200
# [[L,R],[R,1],[R,2],[R,3],[R,4],[R,5],[L,6],[R,7]]=13/7257600
# [[L,R],[R,1],[R,2],[R,3],[R,4],[R,5],[R,6],[L,7]]=1/2419200

# [[L,R],[R,1],[R,2],[R,3],[R,4],[R,5],[L,6],[L,7]]=-713/580608000+5/6144/Pi^6*zeta[3]^2-15/4096/Pi^8*zeta[3]*zeta[5]+29/10240/Pi^8*zeta[3,5]
# [[L,R],[R,1],[R,2],[R,3],[R,4],[L,5],[R,6],[L,7]]=2447/232243200-53/12288/Pi^6*zeta[3]^2-225/8192/Pi^8*zeta[3]*zeta[5]-97/4096/Pi^8*zeta[3,5]
# [[L,R],[R,1],[R,2],[R,3],[L,4],[R,5],[R,6],[L,7]]=-30437/2322432000+71/12288/Pi^6*zeta[3]^2+335/8192/Pi^8*zeta[3]*zeta[5]+359/10240/Pi^8*zeta[3,5]
# [[L,R],[R,1],[R,2],[L,3],[R,4],[R,5],[R,6],[L,7]]=7/5120/Pi^8*zeta[3,5]+65/1024/Pi^8*zeta[3]*zeta[5]-12401/6967296000-17/4096/Pi^6*zeta[3]^2
# [[L,R],[R,1],[L,2],[R,3],[R,4],[R,5],[R,6],[L,7]]=2021/217728000+13/8192/Pi^6*zeta[3]^2-635/8192/Pi^8*zeta[3]*zeta[5]-193/10240/Pi^8*zeta[3,5]
# [[L,R],[R,1],[R,2],[R,3],[R,4],[L,5],[L,6],[R,7]]=319/20480/Pi^8*zeta[3,5]+7/4096/Pi^6*zeta[3]^2-1067/108864000+475/8192/Pi^8*zeta[3]*zeta[5]
# [[L,R],[R,1],[R,2],[R,3],[L,4],[R,5],[L,6],[R,7]]=54991/3483648000-41/6144/Pi^6*zeta[3]^2-145/8192/Pi^8*zeta[3]*zeta[5]+31/5120/Pi^8*zeta[3,5]
# [[L,R],[R,1],[R,2],[L,3],[R,4],[R,5],[L,6],[R,7]]=82211/3483648000+77/6144/Pi^6*zeta[3]^2-645/2048/Pi^8*zeta[3]*zeta[5]-1153/10240/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[R,3],[R,4],[R,5],[L,6],[R,7]]=-49829/2322432000-131/24576/Pi^6*zeta[3]^2+2045/8192/Pi^8*zeta[3]*zeta[5]+469/5120/Pi^8*zeta[3,5]
# [[L,R],[R,1],[R,2],[R,3],[L,4],[L,5],[R,6],[R,7]]=56411/6967296000+19/4096/Pi^6*zeta[3]^2-445/4096/Pi^8*zeta[3]*zeta[5]-379/10240/Pi^8*zeta[3,5]
# [[L,R],[R,1],[R,2],[L,3],[R,4],[L,5],[R,6],[R,7]]=-243947/6967296000-151/12288/Pi^6*zeta[3]^2+415/1024/Pi^8*zeta[3]*zeta[5]+1373/10240/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[R,3],[R,4],[L,5],[R,6],[R,7]]=20711/995328000+1/1024/Pi^6*zeta[3]^2-655/4096/Pi^8*zeta[3]*zeta[5]-703/10240/Pi^8*zeta[3,5]
# [[L,R],[R,1],[R,2],[L,3],[L,4],[R,5],[R,6],[R,7]]=23983/3483648000+1/1536/Pi^6*zeta[3]^2-115/2048/Pi^8*zeta[3]*zeta[5]-107/5120/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[R,3],[L,4],[R,5],[R,6],[R,7]]=1021/129024000+13/2048/Pi^6*zeta[3]^2-215/2048/Pi^8*zeta[3]*zeta[5]-83/5120/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[L,3],[R,4],[R,5],[R,6],[R,7]]=-2633/870912000-7/3072/Pi^6*zeta[3]^2+55/1024/Pi^8*zeta[3]*zeta[5]+29/2560/Pi^8*zeta[3,5]


# [[L,R],[R,1],[R,2],[R,3],[R,4],[L,5],[L,6],[L,7]]=3167/1741824000-13/6144/Pi^6*zeta[3]^2+55/4096/Pi^8*zeta[3]*zeta[5]-27/10240/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[L,3],[L,4],[R,5],[R,6],[R,7]]=20879/3483648000+17/12288/Pi^6*zeta[3]^2-55/1024/Pi^8*zeta[3]*zeta[5]-33/2560/Pi^8*zeta[3,5]
# [[L,R],[R,1],[R,2],[L,3],[R,4],[L,5],[R,6],[L,7]]=-349693/13934592000-5/2048/Pi^6*zeta[3]^2+3615/16384/Pi^8*zeta[3]*zeta[5]+2289/40960/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[R,3],[L,4],[R,5],[L,6],[R,7]]=22427/1990656000-355/49152/Pi^6*zeta[3]^2+65/1024/Pi^8*zeta[3]*zeta[5]+417/10240/Pi^8*zeta[3,5]

# [[L,R],[R,1],[R,2],[R,3],[L,4],[L,5],[L,6],[L,7]]=169/290304000+1/512/Pi^6*zeta[3]^2-15/512/Pi^8*zeta[3]*zeta[5]+3/5120/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[L,3],[L,4],[L,5],[R,6],[R,7]]=-3701/6967296000+1/8192/Pi^6*zeta[3]^2+15/2048/Pi^8*zeta[3]*zeta[5]-27/20480/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[L,3],[R,4],[L,5],[R,6],[L,7]]=-13/580608000-103/49152/Pi^6*zeta[3]^2+55/8192/Pi^8*zeta[3]*zeta[5]-729/40960/Pi^8*zeta[3,5]

# [[L,R],[R,1],[R,2],[L,3],[L,4],[L,5],[L,6],[L,7]]=-1021/870912000+5/256/Pi^8*zeta[3]*zeta[5]-1/1536/Pi^6*zeta[3]^2-1/1280/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[R,3],[L,4],[L,5],[L,6],[L,7]]=137/54432000+1/3072/Pi^6*zeta[3]^2-25/1024/Pi^8*zeta[3]*zeta[5]+17/1280/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[L,3],[R,4],[L,5],[L,6],[L,7]]=-55/2048/Pi^8*zeta[3]*zeta[5]-2909/3483648000-169/5120/Pi^8*zeta[3,5]+5/2048/Pi^6*zeta[3]^2
# [[L,R],[R,1],[L,2],[L,3],[L,4],[R,5],[L,6],[L,7]]=1601/2322432000+85/4096/Pi^8*zeta[3]*zeta[5]+13/640/Pi^8*zeta[3,5]-5/2048/Pi^6*zeta[3]^2
# [[L,R],[R,1],[L,2],[L,3],[L,4],[L,5],[R,6],[L,7]]=-1387/331776000+1/8192/Pi^6*zeta[3]^2+185/8192/Pi^8*zeta[3]*zeta[5]+71/20480/Pi^8*zeta[3,5]
# [[L,R],[R,1],[L,2],[L,3],[L,4],[L,5],[L,6],[R,7]]=9907/3483648000+5/24576/Pi^6*zeta[3]^2-95/8192/Pi^8*zeta[3]*zeta[5]-67/20480/Pi^8*zeta[3,5]

# [[L,R],[R,1],[L,2],[L,3],[L,4],[L,5],[L,6],[L,7]]=-1/2419200


# [[L,R],[R,1],[L,2],[1,3],[R,4],[L,5],[4,6],[L,7]]=74633/7962624000-5183/65536/Pi^6*zeta[3]^2+27055/32768/Pi^8*zeta[3]*zeta[5]+24751/81920/Pi^8*zeta[3,5]


# some order 10 weights:

# [[L,R],[R,1],[R,2],[R,3],[R,4],[R,5],[R,6],[R,7],[R,8],[R,9]]=-1/47900160
# [[L,R],[R,1],[R,2],[R,3],[R,4],[R,5],[R,6],[R,7],[L,8],[L,9]]=(-69/229376*zeta[3,7]-5/24576*Pi^2*zeta[3]*zeta[5]+1/98304*Pi^4*zeta[3]^2+391/458752*zeta[5]^2+186899/12875563008000*Pi^10+17/245760*Pi^2*zeta[3,5])/Pi^10
# [[L,R],[R,1],[R,2],[R,3],[R,4],[R,5],[L,6],[R,7],[L,8],[L,9]]=-2793631/8583708672000+31/196608/Pi^6*zeta[3]^2+31/49152/Pi^8*zeta[3]*zeta[5]+761/491520/Pi^8*zeta[3,5]-63/16384/Pi^10*zeta[3]*zeta[7]+2053/458752/Pi^10*zeta[5]^2-527/229376/Pi^10*zeta[3,7]
# [[L,R],[R,1],[L,2],[R,3],[L,4],[R,5],[L,6],[R,7],[L,8],[R,9]]=4483691/68669669376000-45791/11796480/Pi^6*zeta[3]^2+14687/196608/Pi^8*zeta[3]*zeta[5]+25717/1966080/Pi^8*zeta[3,5]-77357/262144/Pi^10*zeta[3]*zeta[7]-\433371/7340032/Pi^10*zeta[5]^2-259747/7340032/Pi^10*zeta[3,7]

# Boris Shoiket, LMP 56:141--149, 2001
wheelInwards := proc(spokes::posint)
	[seq([i+1,spokes+1], i=1..spokes-1), [1,spokes+1], []]=0:
end proc:
# this result only holds for the harmonic propagator!
# with t-dependence:
#[[2,4],[3,4],[1,4],[]]=-3/4*I*(2*T-1)*zeta[3]*(T-1)^2*T^2/Pi^3


# Michel Van den Bergh, doi 10.1007/s10468-009-9161-6
# this result was also mentioned in the paper by Shoiket above, on top of page 148
wheelOutwards := proc(spokes::posint)
	[seq([i+1], i=1..spokes-1), [1], [seq(1..spokes)]]=coeff(series(-1/4*x*cot(x/2), x, spokes+2), x, spokes):
end proc:
# this result only holds for the harmonic propagator!
# with t-dependence:
#[[2],[3],[1],[1,2,3]]=1/8*I*(2*T-1)*(6*T^4-12*T^3+4*T^2+2*T+1)*zeta[3]/Pi^3

# Kontsevich section 6.4.3
treeGraph := proc(m::posint)
	[[seq(cat(p,i), i=1..m)]]=1/m!:
end proc:

# Bernoulli graph (van den Bergh):
bernoulliGraph := proc(k::posint)
	[seq([L,i+1], i=1..k-1),[L,R]] = (-1)^k*bernoulli(k)/k!:
end proc:

# Graphs from doi:10.2140/pjm.2007.229.1 by Ben Amar:
AmarGraph := proc(k::posint, l2::posint)
	if k<l2 then
		error "expected k>=2*l":
	end if:
	[[`if`(k=l2,L,l2+1),2],seq([R,i+1],i=2..(l2-1)),[R,1],seq([R,`if`(i=k,L,i+1)],i=l2+1..k)]
	=`if`(type(k,'odd'),
		zeta[k],
		Zeta(k)
	)/(2*I*Pi)^k
	*`if`(type(k,'even'),
		 1,
		 (1-2*t)*add(binomial(2*i,i)*(t*(1-t))^i, i=0..floor((l2-1)/2))
	):
end proc:

# Tests for m<>2:
TestsM := [
	[[p1]]=1
	,[[2],[1]]=0 # Kontsevich, Lemma 7.3 (note that 7.3 and 7.5 are actually limit cases of 7.4)
	,[[2],[1,p1]]=0 # vanishes also due to Lemma 7.3
	,[[2,p1],[p1]]=0 # due to Kontsevich Lemma 7.5
	,[[3],[3],[1,2]]=0 # due to 7.3
	,[[2],[1,p1,p2]]=0 # due to 7.3
	,[[2,3],[1,3],[]]=0
	,treeGraph(1)
	,treeGraph(2)
	,treeGraph(3)
	,treeGraph(4)
	,treeGraph(5)
	,treeGraph(6)
	,wheelInwards(2)
	,wheelOutwards(2)
	,wheelInwards(3)
	,wheelOutwards(3)
	,wheelInwards(4)
	,wheelOutwards(4)
	,wheelInwards(5)
	,wheelOutwards(5)
	,wheelInwards(6)
	,wheelOutwards(6)
]:


RunTests := proc(Tests::list(equation))
local j, AllGood, Time, Result, Times, Correct:
	for j from 1 to nops(Tests) do
		printf("Test %2d (%d vertices): ", j, nops(lhs(Tests[j]))):

		# Make sure the timings do not depend on preceeding test cases:
		forgetAllSV():
		forgetAll():

		Time := time():
		Result := weight(lhs(Tests[j])):
		Time  := time()-Time:
		Times[j] := Time:
		if simplify(Result - rhs(Tests[j]))=0 then
			Correct[j] := true:
			printf("ok (%.2fs)\n", Time):
			next:
		end if:
		Correct[j] := false:
		printf("WRONG (%.2fs)\n", Time):
		printf(" - result:\n"):
		print(Result):
		printf(" - expected:\n"):
		print(rhs(Tests[j])):
	end do:
	# Print summary:
	printf("\n\n========================================\n"):
	for j from 1 to nops(Tests) do
		printf(" - Test %2d (%d vertices): %s (%.2fs)\n", j, nops(lhs(Tests[j])), `if`(Correct[j], "ok", "wrong"), Times[j]):
	end do:
	printf("========================================\n"):
	printf("Total time: %.2f\n", `+`(entries(Times, 'nolist'))):
	if false in {entries(Correct, 'nolist')} then
		error "At least one test FAILED":
	else
		printf("All tests successful\n"):
	end if:
end proc:

RunTests(Tests[1..37]):
#RunTests(TestsM):






# The bubble-ladder family, which seems to give the largest weights in the class of quadratic graphs:
bubbleLadderGraph := proc(rungs::nonnegint)
	eval([[1,2],[1,3],seq(op([[2*k,2*k+1],[2*k,2*k+2],[2*k+1,2*k],[2*k+1,2*k+3]]), k=1..rungs)], [2*rungs+2=L,2*rungs+3=R]):
end proc:

# "Clever" constructive integration (only keep the number of variables around that the function really depends on; similar to vertex-width 3):
bubbleLadderWeight := proc(rungs::nonnegint)
local V, F, M, J, e, v, r, pre:
global allvars, delta:
	forgetAll():
	for v from 1 to 1+2*rungs do
		delta[z[v]] := 1:
		delta[zz[v]] := -delta[z[v]]:
		delta[1/z[v]] := -delta[z[v]]:
		delta[1/zz[v]] := -delta[zz[v]]:
	end do:

	pre := 1/(2*I)^(2*(1+2*rungs)):
	pre := pre/(1+2*rungs)!/(2*Pi)^(2*(1+2*rungs)):

	# begin with initial vertex
	M := wedge([angle(1,2), angle(1,3)]):
	allvars := [seq(op([z[v],zz[v]]), v=1..3)]:
	M := convert(eval(M, ln=dlog), 'IterRes', allvars[..2]):
	M := svInt(M):
	print(M):

	# Rungs
	for r from 1 to rungs do
		printf("\n\n\nAdding rung %d:\n\n\n", r):
		J := wedge([angle(2*r,2*r+1),angle(2*r,2*r+2),angle(2*r+1,2*r),angle(2*r+1,2*r+3)]):
		allvars := [seq(op([z[v],zz[v]]), v=2*r..2*r+3)]:
		if r=rungs then
			J := eval(J, [z[2*r+2]=0,zz[2*r+2]=0,z[2*r+3]=1,zz[2*r+3]=1]):
			allvars := [seq(op([z[v],zz[v]]), v=2*r..2*r+1)]:
		end if:
		J := convert(eval(J, ln=dlog), 'IterRes', allvars[..4]):
		lprint(J):

		M := M*J:
		lprint(M):

		for v from 2*r to 2*r+1 do
			printf("Integrating vertex %d\n", v):
			M := svInt(M):
			printf("\n\nRESULT:\n\n"):
			lprint(M):
			allvars := allvars[3..]:
		end do:
	end do:
	expand(M*pre):
end proc:
# results:
#weight(bubbleLadder(0)) = 1/2
#weight(bubbleLadder(1)) = -1/188
#weight(bubbleLadder(2)) = 1/345600
#weight(bubbleLadder(3)) = 11/6502809600-Zeta(3)^2/Pi^6/573440
