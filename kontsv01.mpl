# Erik Panzer
# started 14 July 2016

# Code to evaluate the weights in Kontsevich's formula
# based on single-valued integration

interface(errorbreak=2):
read "HyperInt.mpl":
_hyper_check_divergences := false:

infolevel[svInt] := 4:

FREEZE_WEDGES := true:
WEDGE_SIMPLIFY  := true:
COMBINE_ITERRES := true:

WEDGE_CHECK_SAMPLES := 10:

# The largest dimension for which evaluation samples are used in the reduction of wedges to a basis (evaluation samples mean dense matrices)
MAX_DIM_EVAL_SAMPLES := 30:


# propagator family according to Rossi & Willwacher
#  - 'harmonic angle' (Kontsevich's original paper):    t=1/2 (default)
#  - 'logarithmic propagator':                          t=0
# does NOT include the prefactor 1/(2*I*Pi)
Propagator := proc(v, w, t:=1/2)
	(1-t)*(dlog(z[v]- z[w])-dlog(zz[v]- z[w]))
	+   t*(dlog(z[v]-zz[w])-dlog(zz[v]-zz[w]))
end proc:
# Euclidean angle:
# (v,w)->2*(dlog(z[v]- z[w])-dlog(zz[v]-zz[w])):

# We get explicit powers of i*pi from monodromies all over the place, therefore zeta[2] has to be expanded (it is not independent from (i*pi)^2):
zeta[2] := Zeta(2):


# TODO: is calling expand() on each assignment inefficient? Is it necessary?
`index/sparsereduced` := proc(Idx::list, Tbl::table, Entry::list)
local val:
	if (nargs = 2) then
		if assigned(Tbl[op(Idx)]) then Tbl[op(Idx)]:
		else 0: end if:
	else
#		val := Entry[1]:
		val := expand(Entry[1]):
		if val = 0 then
			Tbl[op(Idx)] := evaln(Tbl[op(Idx)]):
		else
			Tbl[op(Idx)] := val:
		end if:
		val:
	end if:
end proc:

forgetAllSV := proc()
	forget(dlogSimplify,wedgeSimplify,dlogsIterRes,Reduction,wedgeRes,axisHlog,zeroInfPeriodOver1,HlogReglimList,fibBasisList,transportTo1,transportToInf):
end proc:

# Only computes the iterated residues, i.e. assumes that all higher poles (and polynomial parts) cancel
iterRes := proc(expr, vars::list)
local res, S, i, p, X, Z, N, P, Q, f, zero, s, pre, z, had:
	if expr=0 then
		return 0:
	elif op(0,expr)=`+` then
		return map(iterRes, expr, vars):
	elif vars=[] then
		return normal(expr):
	end if:
	# Find zeros of denominator
	if op(0,expr)=DEN then
		pre, Z := 1, [expr]:
	elif op(0,expr)=`*` then
		Z, pre := selectremove(w->op(0,w)=DEN, [op(expr)]):
	else
		error "unexpected argument: %1", expr:
	end if:
	Z, Q := selectremove(has, Z, vars[1]):
	pre := `*`(op(pre),op(Q)):
	res := 0:
	Z := map(op, Z):
	had := []:
	for i from 1 to nops(Z) do
		z := -coeff(Z[i], vars[1], 0)/coeff(Z[i], vars[1], 1):
		if z in had then
			error "higher order pole":
		end if:
		had := [op(had), z]:
		X := mul(`if`(j=i, 1/coeff(Z[i], vars[1], 1), DEN(eval(Z[j], vars[1]=z))), j=1..nops(Z)):
		res := res + den(vars[1],z)*iterRes(X*pre, vars[2..]):
	end do:
	return res:
end proc:

conj := proc(expr)
	if op(0,expr)=z then
		zz[op(1,expr)]:
	elif op(0,expr)=zz then
		z[op(1,expr)]:
	else
		conjugate(expr):
	end if:
end proc:

# continuation around var=at and evaluation at zero
HlogLooped := proc(at, w::list, val:=2*Pi*I)
local i, j, res, u:
	if w=[] then
		return 1:
	end if:
	res := 0:
	# loop over subsequences of "at" starting at i and ending at j
	for i from 1 to nops(w) do
		for j from i to nops(w) do
			if w[j]<>at then
				break:
			end if:
			res := res
			+add((-1)^nops(u[2])*Hlog(at, ListTools[Reverse](u[2]))*u[1], u in regtail([[1, w[..i-1]]], at, 0))
			*val^(j-i+1)/(j-i+1)!
			*add(Hlog(at, u[2])*u[1], u in reghead([[1, w[j+1..]]], at, 0)):
		end do:
	end do:
	res:
end proc:

# Hlog after continuation around var=at
HlogContinued := proc(var, w::list, at, val:=2*Pi*I)
	add(`if`(i=0, 1, Hlog(var, w[1..i]))*HlogLooped(at, w[i+1..], val), i=0..nops(w)):
end proc:

# only for functions of zz
monodromy := proc(expr::table, at, res::table)
global allvars:
local z, w, T, X:
	z := allvars[1]:
	for w in indices(expr, 'nolist') do
		X := HlogContinued(conj(z), w[1], conj(at), -2*Pi*I) - Hlog(conj(z), w[1]):
		T := table(sparsereduced):
		fibrationBasis(X, allvars[2..], T):
		fibrationBasisProduct(T, [[], op(w[2..])], res, expr[w]):
	end do:
end proc:

# expr depends on z and zz
# !ASSUMES! that the monodromy is independent of z (analytic in zz)
doubleMonodromy := proc(expr::table, at, res::table)
local z, X, Y, w:
global allvars:
	z := allvars[1]:
	res := table(sparsereduced):
	for w in indices(expr, 'nolist') do
		X := HlogLooped(at, w[1], 2*Pi*I)*HlogContinued(conj(z), w[2], conj(at), -2*Pi*I):
		if w[1]=[] then
			X := X-Hlog(conj(z), w[2]):
		end if:
		if X=0 then next: fi:
		Y := table(sparsereduced):
		fibrationBasis(X, allvars[2..], Y):
		fibrationBasisProduct(Y, [[], op(w[3..])], res, expr[w]):
	end do:
end proc:

halfRes := proc(expr::table, eq::equation, result::table)
local z, at, X, H, p, q, hlog, k, onto, T, Y, u, w, pre, v, weight, ww, WW, W, S, P:
global allvars:
	z, at := op(eq):
	if not (at in {0,infinity}) then
		error "half-residue at %1 not implemented", at:
	end if:
	if at=infinity then
		S := {indices(expr, 'nolist')}:
#		onto := table(sparsereduced):
#		for w in S do
#			onto[w] := eval(expr[w], den=((u,v)->`if`(u=conj(z), -1, den(u,v)))):
#		end do:
#		lprint([entries(onto,'nolist')]):
#		print(numelems(expr), VS, numelems(onto)):
		X := table(sparsereduced):
		for w in map2(op,1,S) do
			T := table(sparsereduced, transportToInf(w, allvars)):
			W := select(v->v[1]=w, S):
			for ww in map2(op,2,W) do
				H := table(sparsereduced):
				for u in transportToInf(ww, allvars[2..]) do
					fibrationBasisProduct(T, [[], op(lhs(u))], H, rhs(u)):

				end do:
		
				WW := select(v->v[2]=ww, W):
				for u in WW do
					# determine prefactor
					pre := selectfun(expr[u], den):
					pre := select(u->op(1,u)=conj(z), pre):
					# residues at infinity=-sum of all residues at finite points, because 1/z^2/(-1/z-s)=1/z/(-1-s*z) has residue -1 at z=0
					pre := subs(seq(v=-1, v in pre), expr[u])*den(conj(z),0):
					if pre = 0 then next: fi:
					fibrationBasisProduct(H, [[], [], op(u[3..])], X, pre):
				end do:
			end do:
		end do:
		halfRes(X, allvars[1]=0, result):
		return:
	end if:
	# Now in case at=0
	for w in {indices(expr, 'nolist')} do
		if not ({op(w[1]), op(w[2])} subset {0,conj(z)}) then
			next:
		end if:
		pre := coeff(expr[w], den(conj(z), conj(at)), 1):
		if pre = 0 then next: fi:
		if w[1]<>[] then
			printf("angle-dependent function at the half-residue:\n"):
			print(Hlog(z, w[1])*Hlog(conj(z), w[2])*pre):
			error "divergent term":
		end if:

		k := nops(w[2])+1:
		q := -(-Pi*I)^k/k!:

		result[w[3..]] := result[w[3..]] + pre*q:
	end do:
end proc:

svRes := proc(expr::table, eq::equation, res::table)
local z, at, X, pre, w, T:
global allvars:
	z, at := op(eq):
	if at in {0,infinity} then
		error "svRes should only be taken at other variables in the upper half plane; not at %1", at:
	end if:
	for w in indices(expr, 'nolist') do
		pre := coeff(expr[w], den(conj(z), conj(at)), 1):
		if pre=0 then next: end if:
		X := add(f[1]*Hlog(at, f[2]), f in reghead([[1,eval(w[1],conj(z)=conj(at))]], at, log(-1/at))):
		X := X*add(f[1]*Hlog(conj(at), f[2]), f in reghead([[1,w[2]]], conj(at), log(-1/conj(at)))):
		T := table(sparsereduced):
		fibrationBasis(X, allvars[3..], T):
		fibrationBasisProduct(T, w[3..], res, pre*(2*Pi*I)):
	end do:
end proc:

# Observation: If the only var-dependent letter in word is var, then the fibrationBasis of Hlog(var, word)  has only Hlogs of var. This is because the reglim_var->0 of such an object is zero whenever word contains a letter different from 0 or z (rescaling as Hlog(1, word_s->s/z) gives something with vanishing limit z->0 in this case)
# If the only letters are 0 and z, then Hlog(z,word) is just an MZV
# So the axis limit only transforms Hlog(z,) and Hlog(zz,)'s, but in doing so does NOT introduce further Hlog's of other variables
axisHlog := proc(var, word::list)
option remember:
global allvars:
local u, T:
	if not (allvars[2] in word) then
		[[1, word]]:
	end if:
	T := table(sparsereduced):
	fibrationBasis(add(u[1]*Hlog(var, u[2]), u in reghead([[1,eval(word, conj(var)=var)]], var, 0)), [var], T):
	[seq([rhs(u), lhs(u)[1]], u in indices(T, 'pairs'))]:
end proc:

axisLimit := proc(expr::table, z, result::table)
local w, S, SS, T, u:
global allvars:
	S := [indices(expr, 'nolist')]:
	SS := {op(map2(op, 1, S))}:
	for w in SS do
		T := table(sparsereduced):
		for u in select(f->f[1]=w, S) do
			T[u[2..]] := eval(expr[u], conj(z)=z):
		end do:
		for u in axisHlog(z, w) do
			fibrationBasisProduct(T, [u[2], []$(nops(allvars)-2)], result, u[1]):
		end do:
	end do:
end proc:

HlogsIntoTable := proc(expr, vars::list, tab::table, pre:=1)
local todo, Hs, ws, v, H, con:
	if op(0,expr)=`+` then
		map(HlogsIntoTable, [op(expr)], vars, tab, pre):
		return:
	end if:
	if op(0,expr)=`*` then
		todo := [op(expr)]:
	else
		todo := [expr]:
	end if:
	Hs, todo := selectremove(f->op(0,f)=Hlog, todo):
	# constants:
	todo,con := selectremove(has, todo, Hlog):
	con := `*`(op(con)):
	if (Hs<>[]) and (todo<>[]) then
#		WARNING("Expanding expression to extract different Hlogs\n"):
		error "Expanding expression to extract different Hlogs\n":
		HlogsIntoTable(expand(expr), vars, tab, pre):
		return:
	end if:
	if nops(todo)=1 then
		# Now Hs=[], so we just have a prefactor
		HlogsIntoTable(todo[1], vars, tab, pre*con):
		return:
	end if:
	if nops(todo)>1 then
#		WARNING("Expanding expression to extract different Hlogs\n"):
		error "Expanding expression to extract different Hlogs\n":
		HlogsIntoTable(expand(`*`(op(todo))), vars, tab, pre*con):
		return:
	end if:
	ws := [[]$nops(vars)]:
	for H in Hs do
		if op(2,H)=[] then
			next:
		end if:
		v := op(1, H):
		v := ListTools[Search](v, vars):
		if v=0 then
			error "unknown variable: %1", op(1,H):
		end if:
		if ws[v]<>[] then
			error "several Hlog-factors for the same variable %1 in %2", vars[v], Hs:
		end if:
		ws[v] := op(2,H):
	end do:
	tab[ws] := tab[ws] + con*pre:
end proc:

partialFractions := proc(expr, var)
local res, S, w:
	S := selectfun(expr, den):
	S := select(w->op(1,w)=var, S):
	res := [0, seq([op(2,w), coeff(expr,w,1)], w in S)]:
	return res:
end proc:

subtract_last_realaxis := false:

# computes a single-valued primitive wrt allvars[1]
svPrim := proc(expr)
local words, w, S, Mon, X, maxWeight, W, Y, z, tab, Prim, u, v:
global allvars, delta, subtract_last_realaxis:

	z := allvars[1]:
	if conj(z)<>allvars[2] then
		error "expecting second variable to be %1, the conjugate of %1, but found %1", conj(z), z, allvars[2]:
	end if:

	if type(expr, table) then
		tab := expr:
	else
		userinfo(2, svInt, `collecting Hlog-coefficients into table`):
		tab := table(sparsereduced):
		HlogsIntoTable(expr, allvars, tab):
	end if:

	# naive primitive (not single-valued):
	Prim := table(sparsereduced):
	for w in indices(tab, 'nolist') do
		Y := tab[w]:
		W := selectfun(Y, den):
		W := select(u->op(1,u)=z, W):
		for u in W do
			v := [[op(2,u), op(w[1])], op(w[2..])]:
			X := coeff(Y, u, 1):
			Prim[v] := Prim[v] + X:
		end do:
	end do:


	userinfo(2, svInt, `naive primitive has`, numelems(Prim), `terms`):



	# Subtract real axis limit (at least in last integration):
	if (nops(allvars)=2) and (subtract_last_realaxis) then
		userinfo(2, svInt, `subtracting real axis limit on (0,1)`):
		tab := table(sparsereduced):
		axisLimit(Prim, z, tab):
		for w in [indices(tab,'nolist')] do
			X := [[], op(w)]:
			Prim[X] := Prim[X] - eval(tab[w], allvars[1]=allvars[2]):
		end do:
		userinfo(2, svInt, ` => primitive after subtraction:`, numelems(Prim), `terms`):
		userinfo(2, svInt, ` => weight:`, max(0,seq(`+`(map(nops,w)), w in [indices(Prim,'nolist')]))):
	end if:



	# letters in z:
	words := {seq(op(w[1]), w in indices(Prim, 'nolist'))}:
	if conj(z) in words then
		userinfo(1, svInt, `z-zz occurred`):
	end if:
	# in the upper half-plane:
	S := remove(v->delta[v]=-1, words):
	S := (S intersect {op(allvars)}) union select(v->Im(v)>0, S minus {op(allvars)}):
	userinfo(2, svInt, `singular points of primitive in the upper half plane:`, S):
	userinfo(2, svInt, `singularities not in the upper half plane:`, words minus S):

	# compute monodromies
	for w in S do
		Mon[w] := table(sparsereduced):
		doubleMonodromy(Prim, w, Mon[w]):
		if numelems(Mon[w])=0 then
			maxWeight[w] := -1:
		else
			maxWeight[w] := max({seq(nops(w[1]), w in [indices(Mon[w], 'nolist')])}):
		end if:
		userinfo(2, svInt, `monodromy at`, z=w, `has weight`, maxWeight[w]):
	end do:

	# Important: In the following step, we add monodromies to make the primitive single-valued
	# These monodromies can have new monodromies, where the original (non-sv. primitive) did not have any!
	# example: suppose Prim has only z[1]-Hlog's of the form Hlog(z[1], {0,zz[1],z[2],zz[3]}*), hence the only possible monodromy in the upper half plane is at z[1]=z[2]
	# put when we fix this monodromy, we add integrals like Hlog(z[2], {0,zz[1],z[2],zz[3]}*) which transform into Hlog(zz[1], {0,z[2],zz[3]}*) in the fibration basis.
	# these functions can then have monodromies at z[1]=z[3], which need to be subtracted later on!
	words := select(v->delta[v]>0, {op(allvars[3..])}) minus S:
	userinfo(2, svInt, `further punctures in the upper half-plane:`, words):
	for w in words do
		Mon[w] := table(sparsereduced):
		maxWeight[w] := -1:
	end do:
	S := S union words:
	



	# fix monodromies, highest weight to lowest
	if numelems(S)=0 then
		W := -1:
	else
		W := max(entries(maxWeight, 'nolist')):
	end if:
	userinfo(2, svInt, `cancelling monodromies with antiholomorphic functions`):
	while W>=0 do
		tab := table(sparsereduced):
		for w in S do
			for u in [indices(Mon[w], 'nolist')] do
				if nops(u[1])<W then next: end if:
				X := [[op(u[1]), conj(w)], op(u[2..])]:
				tab[X] := -Mon[w][u]/(-2*Pi*I):
				X := [[], op(X)]:
				Prim[X] := Prim[X]-Mon[w][u]/(-2*Pi*I):
			end do:
		end do:
		for w in S do
			monodromy(tab, w, Mon[w]):
			if numelems(Mon[w])=0 then
				maxWeight[w] := -1:
			else
				maxWeight[w] := max({seq(nops(w[1]), w in [indices(Mon[w], 'nolist')])}):
			end if:
			userinfo(2, svInt, `monodromy at`, z=w,  `has weight`, maxWeight[w]):
		end do:
		W := W-1:
	end do:
	# Finished computation of single-valued primitive
	userinfo(2, svInt, `single-valued primitive has`, numelems(Prim), `terms`):
	return copy(Prim):
end proc:

# Reg_{z->infinity} Hlog(z, word)
# DELTA=signum(Im(z))
zeroInfPeriodOver1 := proc(word::list({0,1}))
local w, n, i:
option remember:
	if word=[] then
		1:
	else
		n := nops(word):
		subs(seq(DELTA^(2*i)=1, i=1..floor(n/2)),seq(DELTA^(2*i+1)=DELTA, i=1..floor((n-1)/2)),add(
			expand(w[1]*zeroInfPeriod(subs([0=-1,1=0], w[2])))
		,w in regtail([seq([zeroOnePeriod(word[i+1..]), word[..i]], i=0..n)], 1, -I*Pi*DELTA))):
	end if:
end proc:

# Reglim_{wrt->0} Hlog(var, word)
HlogReglim := proc(at, word::list, wrt)
local depLet, head, v, n, post, first, last, pre, tail:
global delta:
	if word=[] then
		return 1:
	elif word[1]=at then
		error "divergent Hlog(%1, %2)", at, word:
	elif (at=1) and ({op(word)} subset {0,1}) then
		return zeroOnePeriod(word):
	elif (at=infinity) and ({op(word)} subset {0,-1}) then
		return zeroInfPeriod(word):
	elif (at=infinity) and (word[-1]=0) then
		return add(v[1]*HlogReglim(at, v[2], wrt), v in regtail([[1, word]], 0, 0)):
	end if:
	# letters dependent on wrt:
	depLet := select(has, {op(word)}, wrt):
	if (depLet={}) and (at=-1/wrt) then
		pre := regtail([[1, map(v->-v, word)]], 0, I*Pi*delta[wrt]):
		return add(v[1]*`if`({op(v[2])} subset {-1,0}, zeroInfPeriod(v[2]), Hlog(infinity, v[2])), v in pre):
	end if:
	if depLet={} then
		if eval(at, wrt=0)=0 then
			return 0:
		else
			return Hlog(eval(at, wrt=0), word):
		end if:
	end if:
	if (depLet={wrt}) and (at=1) then
		# shuffle of the longest tail in {0,wrt}
		tail := 0:
		n := nops(word):
		while tail<n do
			if word[n-tail] in {0,wrt} then
				tail := tail+1:
			else
				break:
			end if:
		end do:
		if tail=0 then
			pre := eval(word, wrt=0):
			if {op(post)} subset {0,1} then
				return zeroOnePeriod(post):
			else
				return Hlog(at, pre):
			end if:
		elif tail=n then
			return subs(DELTA=-delta[wrt], zeroInfPeriodOver1(subs(wrt=1, word))):
		end if:
		pre := eval(word[..n-tail-1], wrt=0):
		last := word[n-tail]:
		if {last, op(pre)} subset {0,1} then
			return add(subs(DELTA=-delta[wrt], zeroInfPeriodOver1(subs(wrt=1, word[n-i+1..])))*zeroOnePeriod([op(pre), last, 0$(tail-i)]), i=0..tail):
		else
			return add(subs(DELTA=-delta[wrt], zeroInfPeriodOver1(subs(wrt=1, word[n-i+1..])))*add(v[1]*Hlog(1, [op(v[2]), last]),  v in shuffle([[1, pre]], [[(-1)^(tail-i), [0$(tail-i)]]])), i=0..tail):
		end if:
	elif (depLet={1+wrt}) and (at=1) then
		# This is the case occurring from the expansion at z=1+u, which gives periods int_0^1 {...,1+uu} from the path concatenation
		# shuffle of the longest prefix in {1,1+wrt}
		head := 0:
		n := nops(word):
		while head<n do
			if word[head+1] in {1,1+wrt} then
				head := head+1:
			else
				break:
			end if:
		end do:
		if head=0 then
			post := eval(word, wrt=0):
			if {op(post)} subset {0,1} then
				return zeroOnePeriod(post):
			else
				return Hlog(at, post):
			end if:
		elif head=n then
			return (-1)^n*zeroInfPeriod([seq(`if`(word[n-i]=1, 0, -1), i=0..n-1)]):
		end if:
		post := eval(word[head+2..], wrt=0):
		first := word[head+1]:
		if {first, op(post)} subset {0,1} then
			# in this case here, first=0 and the shuffle regularization is already computed in the lookup tables
			return add((-1)^i*zeroInfPeriod([seq(`if`(word[i-j]=1,0,-1), j=0..i-1)])*zeroOnePeriod([1$(head-i), first, op(post)]), i=0..head):
		else
			return add((-1)^i*zeroInfPeriod([seq(`if`(word[i-j]=1,0,-1), j=0..i-1)])*add(v[1]*Hlog(1, [first, op(v[2])]),  v in shuffle([[1, post]], [[(-1)^(head-i), [1$(head-i)]]])), i=0..head):
		end if:
	else
		error "HlogReglim(%1,%2,%3) not implemented", at, word, wrt:
	end if:
end proc:

# d/dwrt log((z-before)/(z-after))
prependList := proc(z, after, before, wrt)
local f, res:
	res := 0:
	f := simplify(z-before):
	if degree(f,wrt)>1 then
		error "nonlinear polynomial %1 unexpected", f:
	elif degree(f, wrt)=1 then
		res := dlog(factor(-coeff(f,wrt,0)/coeff(f,wrt,1))):
	end if:
	f := simplify(z-after):
	if degree(f,wrt)>1 then
		error "nonlinear polynomial %1 unexpected", f:
	elif degree(f, wrt)=1 then
		res := res - dlog(factor(-coeff(f,wrt,0)/coeff(f,wrt,1))):
	end if:
	return [seq([coeff(res,f,1), op(f)], f in selectfun(res, dlog))]:
end proc:

# prepend Hlog(wrt, w)'s in expr with L=[[n1,z1],...] -> n1*Hlog(wrt, [z1,w])+n2*...
prependHlogs := proc(expr, L::list, wrt)
local Hlogs:
	if L=[] then
		return 0:
	else
		Hlogs := select(h->op(1,h)=wrt, selectfun(expr, Hlog)):
		return add(coeff(expr, h)*add(v[1]*Hlog(wrt, [v[2], op(op(2, h))]), v in L), h in Hlogs) +
		subs(seq(h=0, h in Hlogs), expr)*add(v[1]*Hlog(wrt, [v[2]]), v in L):
	end if:
end proc:


HlogReglimList := proc(var, word, wrt)
local res, X, w, S, v:
option remember:
	X := expand(HlogReglim(var, word, wrt)):
	res := table(sparsereduced):
	if op(0,X)=`+` then
		X := [op(X)]:
	else
		X := [X]:
	end if:
	for w in X do
		if not has(w, Hlog) then
			res[1,[]] := res[1,[]] + w:
			next:
		end if:
		S := selectfun(w, Hlog):
		if nops(S)<>1 then
			error "several or no Hlog's: %1", w:
		end if:
		S := op(S):
		res[op(S)] := res[op(S)] + coeff(w, S, 1):
	end do:
	return [entries(res, 'pairs')]:
end proc:
		

fibBasis := proc(var, word, wrt::list)
local i, n, res, L:
	return add(rhs(w)*mul(`if`(lhs(w)[i]=[], 1, Hlog(wrt[i], lhs(w)[i])), i=1..nops(wrt)), w in fibBasisList(var, word, wrt)):


	if word=[] then
		return 1:
	elif word[1]=var then
		error "divergent Hlog":
	elif (var=1) and ({op(word)} subset {0,1}) then
		return zeroOnePeriod(word):
	elif wrt=[] then
		return Hlog(var, word):
	elif not (has(var, wrt[1]) or has(word, wrt[1])) then
		return fibBasis(var, word, wrt[2..]):
	elif (var=wrt[1]) and (not has(word, wrt[1])) then
		return Hlog(var, word):
	end if:
	# differentiate under the integral and integrate:
	n := nops(word):
	res := fibBasis(HlogReglim(var, word, wrt[1]), wrt[2..]):
	for i from 1 to n do
		L := prependList(word[i], `if`(i=n, 0, word[i+1]), `if`(i=1, var, word[i-1]), wrt[1]):
		if L=[] then
			next:
		end if:
		res := res + 
			prependHlogs(fibBasis(var, subsop(i=NULL, word), wrt), L, wrt[1]):
	end do:
	return collect(res, Hlog):
end proc:

# returns a list of key=value pairs (table initializer)
# key = [w1,...,w_nops(wrt)] corresponds to Hlog(wrt[1], w1)*Hlog(wrt[2], w2)*...
fibBasisList := proc(var, word, wrt::list)
local i, n, res, L, v, w, u:
option remember:
	if word=[] then
		return [[[]$nops(wrt)]=1]:
	elif word[1]=var then
		error "divergent Hlog":
	elif (var=1) and ({op(word)} subset {0,1}) then
		res := zeroOnePeriod(word):
		if res=0 then
			return []:
		else
			return [[[]$nops(wrt)]=res]:
		end if:
	elif wrt=[] then
		return [[] = Hlog(var, word)]:
	elif not (has(var, wrt[1]) or has(word, wrt[1])) then
		return map(v->[[], op(lhs(v))]=rhs(v), fibBasisList(var, word, wrt[2..])):
	elif (var=wrt[1]) and (not has(word, wrt[1])) then
		return [[word, []$(nops(wrt)-1)] = 1]:
	end if:

	n := nops(word):
	res := table(sparsereduced):

	# constant pf integration:
	for w in HlogReglimList(var, word, wrt[1]) do
		for v in fibBasisList(lhs(w), wrt[2..]) do
			u := [[], op(lhs(v))]:
			res[u] := res[u] + rhs(v)*rhs(w):
		end do:
	end do:

	# differentiate under the integral and integrate:
	for i from 1 to n do
		L := prependList(word[i], `if`(i=n, 0, word[i+1]), `if`(i=1, var, word[i-1]), wrt[1]):
		if L=[] then
			next:
		end if:
		for w in fibBasisList(var, subsop(i=NULL, word), wrt) do
			for v in L do
				u := [[v[2], op(lhs(w)[1])], op(lhs(w)[2..])]:
				res[u] := res[u] + v[1]*rhs(w):
			end do:
		end do:
	end do:
	return [indices(res, 'pairs')]:
end proc:

fibBasisListTest := proc(var, word, wrt::list)
local IST, SOLL:
	IST := fibBasis(var, word, wrt):
	SOLL := fibrationBasis(Hlog(var, word), wrt):
	if simplify(IST-SOLL)<>0 then
		error "mismatch for fibBasisList(%1, %2, %3)", var, word, wrt:
	end if:
	return fibBasisList(var, word, wrt):
end proc:


# Write Hlog(1+z, word) in fibration basis wrt vars
# Path concatenation to transform Hlog(1+z, ...) into Hlog(z, ...):
#
#	Hlog(z, map(q->`if`(q=conj(z), conj(z), q-1), w[1][..i]))
#	*(-I*Pi*delta[z])^k/k!
#	*Hlog(1, Reg_0^1 (w[j+1..]))
transportTo1 := proc(word::list, vars::list)
global delta:
local i, j, v, ww, k, X, u, T, z:
option remember:
	z := vars[1]:
	T := table(sparsereduced):
	# sum over tails: Hlog(1, Reg^1(...))
	for j from 1 to nops(word)+1 do
	for v in reghead([[1, word[j..]]], 1, 0) do
		for ww in fibBasisList(1, subs(conj(z)=conj(z)+1,v[2]), vars[2..]) do
			# sum over {1}*-tails word[..i],{1}* of the prefix word[..j-1]
			for i from j-1 to 0 by -1 do
				if i+1<j then
					if word[i+1]<>1 then
						break:
					end if:
				end if:
				k := j-i-1:
				X := (-I*Pi*delta[z])^k/k!*rhs(ww)*v[1]:
				u := [map(q->`if`(q=conj(z),conj(z),q-1), word[..i]), op(lhs(ww))]:
				T[u] := T[u] + X:
			end do:
		end do:
	end do:
	end do:
	return [entries(T, 'pairs')]:
end proc:

# Expand after z->-1/z, zz->-1/zz
transportToInf := proc(word::list, vars::list)
global delta:
local i, v, u, z, res, pre, n:
option remember:
	z := vars[1]:

	pre := [[1, []]]:
	n := nops(word):

	res := table(sparsereduced):

	for i from 0 to n do
		if i=0 then
			pre := [[1, []]]:
		else
			pre := Mul(pre, [[-1, [0]], `if`(word[i]=conj(z), [1,[conj(z)]], NULL)]):
		end if:
		fibrationBasis(map(u->[u[1],`if`(u[2]=[],[],[u[2]])], regtail([[1, map(v->-v, subs(conj(z)=-1/conj(z), word[i+1..]))]], 0, I*Pi*delta[z])), vars[2..], res, table(), [pre]):
	end do:
	return [entries(res, 'pairs')]:
end proc:

# integrates expr wrt. allvars[1..2]=[z,zz] over the upper half-plane
svInt := proc(expr)
local words, w, S, Mon, X, maxWeight, W, prim, Y, T, z, tab, k, j, resTab, Prim, prim1, Prim1, u, v, i, ww:
global allvars, delta:
	z := allvars[1]:
	if allvars[2]<>conj(z) then
		error "expected %1 as second variable, but got %2", conj(z), allvars[2]:
	end if:
	userinfo(2, svInt, `starting integration of`, z, `over the upper half-plane`):

	Prim := svPrim(expr):
	# Prim=fibrationBasis-table of the s.v. primitive

	# Table to store the result of the line integral of Prim:
	resTab := table(sparsereduced):

	# S=poles in the upper half plane:
	S := selectfun({entries(Prim, 'nolist')}, den):
	S := map2(op,2,select(q->op(1,q) = allvars[2], S)):
	S := select(q->delta[q]<0, S intersect {op(allvars)}) union select(q->Im(q)<0, S minus {op(allvars)}):
	S := map(conj, S):
	userinfo(2, svInt, `poles in the upper half plane:`, S):
	# Compute residues at poles:
	for w in S do
		userinfo(2, svInt, `computing residue at`, z=w):
		svRes(Prim, z=w, resTab):
	end do:

	# Expansion around 1 (needed both for half-residue and real-line integration)
	userinfo(2, svInt, `computing expansion near 1`):
	Prim1 := table(sparsereduced):
	S := {indices(Prim, 'nolist')}:
	for w in map2(op,1,S) do
		T := table(sparsereduced, transportTo1(w, allvars)):
		W := select(v->v[1]=w, S): # words beginning with w
		for ww in map2(op,2,W) do
			Y := table(sparsereduced):
			for u in transportTo1(ww, allvars[2..]) do
				fibrationBasisProduct(T, [[], op(lhs(u))], Y, rhs(u)):
			end do:
			for u in select(v->v[2]=ww, W) do
				X := eval(Prim[u], den=((aa,bb)->`if`(aa=conj(z), den(conj(z), bb-1), den(aa,bb)))):
				fibrationBasisProduct(Y, [[], [], op(u[3..])], Prim1, X):
			end do:
		end do:
	end do:
	userinfo(2, svInt, ` =>`, numelems(Prim1), `terms in fibration basis`):

	for w in {0,1,infinity} do
		userinfo(2, svInt, `computing half-residue at`, z=w):
		if w=1 then
			halfRes(Prim1, z=0, resTab):
		else
			halfRes(Prim, z=w, resTab):
		end if:
	end do:

	# Compute integral along the real line
	userinfo(2, svInt, `real axis limit on the interval (0,1):`):
	tab := table(sparsereduced):
	if not ((nops(allvars)=2) and (subtract_last_realaxis)) then
		axisLimit(Prim, z, tab):
	end if:
	S := {indices(tab, 'nolist')}:
	userinfo(2, svInt, ` =>`, numelems(tab), `terms in fibration basis`):
	userinfo(2, svInt, ` =>`, `weight`= max(seq(`+`(map(nops,w)), w in S))):

	userinfo(2, svInt, `line integral 0->1:`):
	for w in map2(op,1,S) do
		# prefactors that appear:
		W := select(v->v[1]=w, S):
		Y := {seq(op(selectfun(tab[v], den)), v in W)}:
		Y := select(v->op(1,v)=z, Y):
		for i in Y do
			T := table(sparsereduced):
			for v in reghead([[1, [op(2,i), op(w)]]], 1, 0) do
			for ww in fibBasisList(1, v[2], allvars[3..]) do
				u := lhs(ww):
				T[u] := T[u] + rhs(ww)*v[1]:
			end do:
			end do:
			for v in W do
				fibrationBasisProduct(T, v[2..], resTab, coeff(tab[v], i, 1)):
			end do:
		end do:
	end do:
	userinfo(2, svInt, ` =>`, numelems(resTab), `terms in total`):
	userinfo(2, svInt, ` =>`, `weight`= max(seq(`+`(map(nops,w)), w in {indices(resTab, 'nolist')}))):


	userinfo(2, svInt, `real axis limit on the interval (-infinity,0):`):
	# Analytic continuation around zero is trivial; only the logs (trailing zeros) get I*Pi*delta[z]:
	X := table(sparsereduced):
	for w in indices(Prim, 'nolist') do
		for i from 0 to nops(w[1]) do
			if (i>0) then
				if w[1][-i]<>0 then break: fi:
			end if:
		for j from 0 to nops(w[2]) do
			if (j>0) then
				if w[2][-j]<>0 then break: fi:
			end if:
			ww := [map(f->-f,eval(w[1][..-1-i], conj(z)=-conj(z))), map(f->-f,w[2][..-1-j]), op(w[3..])]:
			X[ww] := X[ww] + eval(Prim[w], den=((aa,bb)->`if`(aa=conj(z), -den(conj(z), -bb), den(aa,bb))))*(I*Pi*delta[z])^i/i!*(I*Pi*delta[conj(z)])^j/j!:
		end do:
		end do:
	end do:
	tab := table(sparsereduced):
	axisLimit(X, z, tab):
	userinfo(2, svInt, ` =>`, numelems(tab), `terms in fibration basis`):
	userinfo(2, svInt, ` =>`, `weight`= max(seq(`+`(map(nops,w)), w in [indices(tab,'nolist')]))):
	userinfo(2, svInt, `line integral 0->-infinity:`):
	fibreIntegration(tab, subsop(2=NULL, allvars), resTab):
	userinfo(2, svInt, ` =>`, numelems(resTab), `terms in total`):
	userinfo(2, svInt, ` =>`, `weight`= max(seq(`+`(map(nops,w)), w in {indices(resTab, 'nolist')}))):


	userinfo(2, svInt, `real axis limit on the interval (1,infinity):`):
	tab := table(sparsereduced):
	axisLimit(Prim1, z, tab):
	userinfo(2, svInt, ` =>`, numelems(tab), `terms in fibration basis`):
	userinfo(2, svInt, ` =>`, `weight`= max(seq(`+`(map(nops,w)), w in [indices(tab,'nolist')]))):

	userinfo(2, svInt, `line integral 1->infinity:`):
	fibreIntegration(tab, subsop(2=NULL, allvars), resTab):
	userinfo(2, svInt, ` =>`, numelems(resTab), `terms in total`):
	userinfo(2, svInt, ` =>`, `weight`= max(seq(`+`(map(nops,w)), w in {indices(resTab, 'nolist')}))):

	userinfo(2, svInt, `preparing result expression as a sum`):
	return add(rhs(w)*mul(`if`(lhs(w)[k-2]=[],1,Hlog(allvars[k], lhs(w)[k-2])), k=3..nops(allvars)), w in entries(resTab, 'pairs')):
end proc:

# normalize dlogs such that the coefficient of the first variable in the argument is 1
# that is, this identifies dlog(a-b) with dlog(b-a)
# for this it does not matter where the order comes from; just take Maple's own automatic order in a set
dlogSimplify := proc(expr)
local S:
option remember:
	S:=indets(expr):
	if S={} then
		return 0:
	else
		return dlog(expr/coeff(expr,S[1],1)):
	end if:
end proc:

wedgeSimplify := proc(forms::list)
local i, j, k, L, f, pre, S, c, run, runMax, cMax:
option remember:
global WEDGE_SIMPLIFY:
	if forms=[] then
		return 1:
	end if:
	# pull out a common factor and the denominator of all dlog-coefficients and choose the sign such that the "first" dlog has positive coefficient
	pre := 1:
	L := []:
	for i from 1 to nops(forms) do
		f := eval(forms[i], dlog=dlogSimplify):
		S := selectfun(f, dlog):
		if S={} then
			return 0:
		end if:
		S := S[1]:
		c := coeff(f, S, 1):
		pre := pre*c:
		f := f/c:
		pre := pre/denom(f):
		f := numer(f):
		L:=[op(L), expand(f)]:
	end do:
	# Equal forms?
	if nops({op(L)})<nops(L) then
		return 0:
	end if:

	if WEDGE_SIMPLIFY then

	# if there is a form involving only one differential, can remove all forms that only depend on this differential from all other factors:
	for i from 1 to nops(L) do
		S := indets(map(op,selectfun(L[i], dlog))):
		if nops(S)=0 then
			error "no variables in %1", L[i]:
		elif nops(S)>1 then
			next:
		end if:
		c := S[1]:
		S := selectfun(subsop(i=NULL, L), dlog):
		S := select(j->indets(op(j))={c}, S):
		if S<>{} then
			f := map(j->j=0, S):
			S := wedgeSimplify([seq(eval(L[j], f), j=1..i-1), L[i], seq(eval(L[j], f), j=i+1..nops(L))]):
			userinfo(4, svInt, `simplification: lonely differential=`, c, print(wedge(L)=S)):
			return pre*S:
		end if:
	end do:
	# are there slots with only one form? 
	# If so, can remove this form from all other slots
	for i from 1 to nops(L) do
		S := selectfun(L[i], dlog):
		if nops(S)=1 then
			S := S[1]:
			if has(subsop(i=NULL, L), S) then
				S := wedgeSimplify(subsop(i=L[i], eval(L, S=0))):
				userinfo(4, svInt, `simplification: lonely form=`, L[i], print(wedge(L)=S)):
				return pre*S:
			end if:
		end if:
	end do:
	# if the dlog's of a form are a subset of the dlog's of another, we can kill at least one term:
	for i from 1 to nops(L) do
		S := selectfun(L[i], dlog):
		for j from 1 to nops(L) do
			if j=i then
				next:
			end if:
			if not (S subset selectfun(L[j], dlog)) then
				next:
			end if:
			# choose the multiple of L[i] that cancels as many forms in L[j] as possible
			S := [seq(coeff(L[j], c, 1)/coeff(L[i], c, 1), c in S)]:
			S := sort(S):
			c := S[1]:
			run := 1:
			runMax := 0:
			for k from 2 to nops(S) do
				if S[k]=c then
					run := run+1:
				else
					if run>runMax then
						runMax := run:
						cMax := c:
					end if:
					c := S[k]:
					run := 1:
				end if:
			end do:
			if run>runMax then
				runMax := run:
				cMax := c:
			end if:
			S := wedgeSimplify(subsop(j=L[j]-cMax*L[i], L)):
			userinfo(4, svInt, `wedge simplification: [runMax, cMax]=`, [runMax,cMax], print(wedge(L)=S)):
			return pre*S:
		end do:
	end do:

	end if:

	# sort the forms
	L, c := sort(L, 'output=[sorted,permutation]'):
	pre := pre*GroupTheory[PermParity](Perm(c)):
	return pre*wedge(L):
end proc:

dlogsIterRes := proc(forms::list, vars::list)
option remember:
local poles, res, z, i, j, w, L, pole, cs, todo:
global COMBINE_ITERRES:
	if 0 in forms then
		return 0:
	elif vars=[] then
		if forms=[] then
			return 1:
		else
			return wedgeSimplify(forms):
		end if:
	elif forms=[] then
		# the constant 1 does not have any residues
		return 0:
	end if:

	# expand linear combinations
#	for k from 1 to nops(w) do
#		if op(0,w[k])=`+` and false then
#			printf("Expanding sum %a (at %d) in:\n", w[k], k):
#			return add(coeff(w[k],z,1)*dlogsIterRes(subsop(k=z, w), vars), z in selectfun(w[k], ln)):
#		end if:
#	end do:

	res := 0:
	poles := select(w->has(w, vars[1]), selectfun(forms, dlog)):
	for pole in poles do
		# singularity wrt vars[1] at:
		z := op(pole):
		z := -coeff(z, vars[1], 0)/coeff(z,vars[1],1):
		# which of the forms have this residue?
		cs := map(coeff, forms, pole, 1):

		L := eval(forms, pole=0):
		L := eval(L, vars[1]=z):

		todo := select(j->cs[j]<>0, {seq(1..nops(forms))}):
		if todo={} then
			error "dlog(%1) present but with zero coefficient", vars[1]-z:
		end if:
		while (nops(todo)>=2) and COMBINE_ITERRES do
			# special case with precisely two forms: combine wedges by linearity! (one term instead of two)
			i, j := min(todo), max(todo):
			todo := todo minus {i,j}:

#			w := cs[i]*(-1)^(i-1)*wedge(subsop(k=NULL, w))
#			   + cs[j]*(-1)^(j-1)*wedge(subsop(k=NULL, w)):
			w := (-1)^(j-1)*wedgeSimplify(subsop(i=L[i]*cs[j]-L[j]*cs[i], j=NULL, L)):

			w := convert(w, 'IterRes', vars[2..]):
			res := res+den(vars[1], z)*w:

			next:
		end do:
		# general case
		for i in todo do
			w := cs[i]*(-1)^(i-1)*wedgeSimplify(subsop(i=NULL, L)):

			w := convert(w, 'IterRes', vars[2..]):
			res := res+den(vars[1], z)*w:

		end do:
	end do:
	res:
end proc:

`convert/IterRes` := proc(expr, vars::list)
	eval(expr, wedge=(L->dlogsIterRes(L, vars))):
end proc:

# I do not want to remove {z[i]}'s, because they are not taken care of separately in the reduction
irreducibles := proc(S)
	# do not remove variables like z[i], as they should be counted for the best order search!
	# simply normalize the coefficient of the "smallest" variable:
	map(p->`if`(indets(p)={}, NULL, p/coeff(p, indets(p)[1], 1)), S):
end proc:

Reduction := proc(vars::list)
option remember:
local Cs, c, p, H, i, j, zero:
global bubbles:
	Cs := {}:
	for c in Reduction(vars[..-2]) do
		for p in c do
			if degree(p, vars[-1])=0 then
				next:
			end if:
			zero := solve(p, vars[-1]):
			i := op(1, vars[-1]):
			if zero=0 then
				j := "L":
			elif zero=1 then
				j := "R":
			else
				j := op(1, zero):
			end if:
			# in the very first reduction, z[j]-zz[j] can only appear if we have two propagators connecting i with j:
			# TODO: this could be further improved! There are spurios z-zz from later reduction stages... (have to keep track which pairs of letters come bundled in a propagator...)
			if (nops(vars)<=2) and (not ({i,j} in bubbles)) then
				H := {irreducibles(eval(remove(let->map2(op,1,indets(let))={i,j}, c), vars[-1]=zero))}:
			else
				H := {irreducibles(eval(c minus {p}, vars[-1]=zero))}:
			end if:
			Cs := Cs union H:
		end do:
	end do:
	# remove cliques which are contained in a bigger clique
	p := Cs:
	for c in p do
		Cs := remove(v->(nops(v)<nops(c)) and (v subset c), Cs):
	end do:
	Cs:
end proc:

score := proc(vars::list)
	[seq(nops(select(has, `union`(op(Reduction(vars[..i-1]))), vars[i])), i=1..nops(vars))]:
end proc:

bestOrder := proc(G::list(list))
local S, Red, tau, V, vars, M, Best, i, better, Poles:
	V := nops(map(op, {op(G)}) minus {'L', 'R'}):
	Poles := irreducibles(eval(map(op, selectfun(map(Propagator@op, G), dlog)), [z['L']=0,zz['L']=0,z['R']=1,zz['R']=1])):
	forget(Reduction):
	Reduction([]) := {Poles}:
	Best := {}:

	bettereq := (a,b)->`and`(seq(a[i]<=b[i], i=1..nops(vars))):

	for tau in combinat[permute](V) do
		printf("order %a:", tau):
		vars := [seq(op([z[i],zz[i]]), i in tau)]:
		M := score(vars):
		# do we have a better score already?
		if select(S->bettereq(rhs(S), M), Best)<>{} then
			next:
		end if:
		Best := remove(S->bettereq(M,rhs(S)), Best) union {tau=M}:
	end do:
	# the maximal number of letters is 2*V-1, so we can interpret the list of letters as a base 2*V number
	Best := map(S->((add((2*V)^i*rhs(S)[nops(vars)-i], i=0..nops(vars)-1),rhs(S))=lhs(S)), Best):
	print(Best):
	rhs(Best[1]):
end proc:

# Faster computation of reductions by lexorder and early pruning
bestOrder := proc(G::list(list))
local S, tau, V, vars, M, Best, i, Poles, top, tops:
global bubbles:
	V := map(op, {op(G)}) minus {'L', 'R'}:
	Poles := irreducibles(eval(map(op, selectfun(map(Propagator@op, G), dlog)), [z['L']=0,zz['L']=0,z['R']=1,zz['R']=1])):

	# 'bubbles' stores all anti-parallel edge pairs:
	bubbles := select(top->(([top[1], top[2]] in G) and ([top[2], top[1]] in G)), combinat[choose](V, 2)):

	forget(Reduction):
	Reduction([]) := {Poles}:

	Best := {[]}:

	for i from 1 to nops(V) do
		Best := `union`(seq({seq([op(tau), v], v in (V minus {op(tau)}))}, tau in Best)):
		top := infinity:
		tops := {}:
		for tau in Best do
			printf("order %a:\n", tau):
			vars := [seq(op([z[i],zz[i]]), i in tau)]:
			M := score(vars):
			# turn list of alphabet sizes into one big number: (base = 2*nops(V) = max. alphabet size)
			M := add((2*nops(V))^(nops(vars)-i)*M[i], i=1..nops(vars)):
			if M>top then
				next:
			elif M<top then
				top := M:
				tops := {tau}:
			else
				tops := tops union {tau}:
			end if:
		end do:
		Best := tops:
	end do:
	print(Best):
	Best[1]:
end proc:

# evaluate the rational function f, defined by
# forms[1] wedge ... wedge forms[k] = f * d(z1) wedge ... wedge d(zk)
# for the arguments zk=atk where at= [[z1,at1],...,[zk,atk]]
# NOTE: exact arithmetic
wedgeEval := proc(forms::list, at::list({symbol,indexed}=numeric))
uses LinearAlgebra:
local M, i, j:
	if nops(forms)<>nops(at) then
		error "number of forms = %1 differs from count of variables %2", nops(forms), at:
	end if:
	M := Matrix(nops(forms), nops(forms), [seq([seq(eval(diff(eval(forms[i],dlog=ln), lhs(at[j])), at), j=1..nops(at))], i=1..nops(forms))]):
	Determinant(M):
end proc:

# evaluate the function f mod p
wedgeEvalMod := proc(forms::list, at::list({symbol,indexed}=numeric), modulus::posint)
uses LinearAlgebra[Modular]:
local i, j:
	if nops(forms)<>nops(at) then
		error "number of forms = %1 differs from count of variables %2", nops(forms), at:
	end if:
	Determinant(modulus, Mod(modulus, [seq([seq(eval(diff(eval(forms[i],dlog=ln), lhs(at[j])), at), j=1..nops(at))], i=1..nops(forms))], integer[])):
end proc:

# create a configuration (xi<>xj for i<>j) of N variables, not 0 or 1, drawing values by calling ran()
sampleConfiguration := proc(N::integer, ran::procedure)
local j, got, z:
	got := [0,1]:
	for j from 1 to N do
		z := ran():
		while z in got do
			z := ran():
		end do:
		got := [op(got), z]:
	end do:
	got[3..]:
end proc:

# compute iterated residues of a wedge product of dlog's
# expects that dlog's are written as dlog({a,b})
# returns an integer
wedgeRes := proc(forms::list, poles::list)
local L, M, i, j, var, at:
option remember:
	L := forms:
	M := Matrix(1..nops(forms), 1..nops(poles)):
	for i from 1 to nops(poles) do
		var, at := op(poles[i]):
		for j from 1 to nops(forms) do
			M[i,j] := coeff(L[j], dlog({var,at}), 1):
		end do:
		L := eval(L, var=at):
	end do:
	M := LinearAlgebra[Determinant](M):
	return M:
end proc:

# sample iterated residue
# only produces residues that a function with denominators in "Poles" can possibly have
# uniform among those (in some sense; see TODO below)
sampleResidue := proc(Poles::set, vars::set)
local at, var, pole:
	if not (vars subset `union`(op(Poles))) then
		return false:
	end if:
	if nops(Poles)<nops(vars) then
		return false:
	end if:
	if vars={} then
		if Poles<>{} then
			error "left over singularities: %1", Poles:
		end if:
		return []:
	end if:

	# pick a divisor at random
	pole := Poles[1+(rand() mod nops(Poles))]:
	# pick a variable at random
	var := vars intersect pole:
	if var={} then
		error "constant divisor: %1 independent of %2", pole, vars:
	end if:
	if nops(var)=1 then
		var := var[1]:
	else
		var := var[1+(rand() mod 2)]:
	end if:
	at := op(pole minus {var}):
	# TODO: first picking variable and then picking divisor would give a different sample distribution! Which is better?

	# Polynomial reduction (after removing constants):
	pole := sampleResidue(remove(k->indets(k)={}, eval(Poles minus {pole}, var=at)), vars minus {var}):
	if type(pole, 'list') then
		return [var=at, op(pole)]:
	else
		return false:
	end if:
end proc:

# checks a relation wedge(...) = linear combination of other wedges
# by evaluating at random integer configurations
checkWedgeRelation := proc(expr::equation)
global WEDGE_CHECK_SAMPLES, WEDGE_CHECK_RAN:
local k, X, wedges, vars, sample, w, test, deg:
	if not assigned(WEDGE_CHECK_RAN) then
		WEDGE_CHECK_RAN := rand(50):
	end if:
	X := lhs(expr)-rhs(expr):
	wedges := selectfun(X, wedge):
	deg := nops(op(wedges[1])):
	vars := [op(indets(map(op, selectfun(X, dlog))))]:
	if nops(vars)<deg then
		# in this case, expr should be wedge()=0
		if expr<>(wedges[1]=0) then
			error "wedge with too few variables; simple wedge=0 expected, but got %1", expr:
		end if:
		return:
	end if:
	if nops(vars)>deg then
		error "too many variables in %1", expr:
	end if:
	for k from 1 to WEDGE_CHECK_SAMPLES do
		sample := sampleConfiguration(nops(vars), WEDGE_CHECK_RAN):
		sample := [seq(vars[i]=sample[i], i=1..nops(vars))]:
		test := add(coeff(X, w, 1)*wedgeEval(op(w), sample), w in wedges):
		test := simplify(test):
		if test<>0 then
			printf("Testing wedge relation:\n"):
			print(expr):
			printf("at the point:\n"):
			print(sample):
			printf("LHS-RHS = %a\n", test):
			error "wedge relation failed!":
		end if:
	end do:
end proc:

# choose a basis among the wedges
toWedgeBasis := proc(expr)
local modulus, samples, vars, v, Nsamples, i, ran, j, s, got, z, M, B, dim, DIM, MM, SUBSTI, RHS, SOL, V, Poles, BasisForms, k, allzero, iDens, ResidueSamples, Q, P, PoleList, SampleRange:
global allvars:
global WedgeRelations:
global MAX_DIM_EVAL_SAMPLES:
uses LinearAlgebra:
	WedgeRelations := table():

	V := [op(selectfun(expr, wedge))]:
	if V=[] then
		return expr:
	end if:

	# sort V's by complexity (want to choose basis among the simplest):
	V := sort(V, length):
	userinfo(2, svInt, `Picking basis among wedge(...)'s:`):
	userinfo(2, svInt, ` - in input:`, nops(V)):

	# remaining variables:
	vars := allvars[3..]:
	# ambient dimension (dimension of logarithmic volume forms):
	DIM  := (nops(vars)+1)!:
	userinfo(2, svInt, ` - ambient dimension:`, DIM):

	# all singularities in the input: dlog(a-b) -> {a,b}
	Poles := map(op, selectfun(V, dlog)):
	Poles := {seq(seq({i, solve(j,i)}, j in select(has, Poles, i)), i in allvars)}:
	userinfo(2, svInt, ` - poles of input functions:`, print(Poles)):
	# Compute upper bound on the non-zero iterated residues:
	dim := 1:
	for v in vars do
		PoleList[v] := map(op,select(has, Poles, v)) minus {v}:
		dim := dim*nops(PoleList[v]):
		Poles := `union`(seq(eval(Poles,v=z), z in PoleList[v])) minus {{v,v}}:
	end do:
	userinfo(2, svInt, ` - locations for potential iterated residues:`, print(seq(v=PoleList[v], v in vars))):
	userinfo(2, svInt, ` - resulting upper bound on dimension:`, dim):
	if dim<DIM then
		DIM := dim:
	end if:
	if DIM=0 then
		for v in V do
			checkWedgeRelation(v=0):
			WedgeRelations[op(v)] := 0:
		end do:
		return eval(expr, wedge=0):
	end if:


	# extract the denominators appearing anywhere:
	iDens := lcm(op(map(denom, map(op@op, V)))):
	userinfo(2, svInt, ` - LCM of denominators:`, iDens):
	modulus := nextprime(2^20):
	# make sure all rational coefficients are defined:
	while (iDens mod modulus) = 0 do
		modulus := nextprime(modulus):
	end do:
	userinfo(2, svInt, ` - modulus`, modulus):


	# To compute the rank and pick a basis, we need to construct a sufficient supply of functionals
	#   case A: abs(V) is above or close to DIM -> we have to take the full basis
	#   case B: abs(V) is much smaller than DIM -> we generate random functionals
	# Applying B in the case A would be very inefficient

	if (nops(V)>DIM*2/3) or (DIM<10) then
		# case A: take all iterated residues as a basis of functionals
		#         e.g. decompose into iterated partial fractions
		ResidueSamples := true:

		Nsamples := dim:
		samples := table():
		for i from 0 to dim-1 do
			s := []:
			k := i:
			for v in vars do
				s := [op(s), v=PoleList[v][1+irem(k, nops(PoleList[v]))]]: 
				k := iquo(k, nops(PoleList[v])):
			end do:
			samples[1+i] := s:
		end do:
		userinfo(2, svInt, ` - generated all iterated residues`):

		# rewrite dlog(a-b) as dlog({a,b}): (needed for wedgeRes)
		BasisForms := eval(V, dlog=(foo->dlog({indets(foo)[1], solve(foo, indets(foo)[1])}))):
		# columns are the forms, rows the sample points
		M := LinearAlgebra[Modular][Mod](modulus, [seq([seq(
			wedgeRes(
				op(BasisForms[i]),
				samples[j]
			) mod modulus
		, i=1..nops(V))], j=1..Nsamples)], integer[]):
	else
		# case B: much less wedge()'s than ambient dimension
		#         generate random functionals
		ResidueSamples := false:

		dim := min(nops(V), DIM): # upper bound for rank
		userinfo(2, svInt, ` - upper bound on the rank:`, dim):

		# for point evaluation functionals, all variables must get different values, and different from 0 and 1
		SampleRange := nops(vars)+2+6:
		userinfo(2, svInt, ` - samples drawn from the interval`, [0,SampleRange-1]):
		ran := rand(SampleRange):


		# Pick number of samples such that the probability of deficient rank is less than 1/10^8, if we were working over the finite field with SampleRange elements (actually, our modulus will be much bigger):
		Nsamples := ceil(dim-1+8*log[SampleRange](10)):
		userinfo(2, svInt, `going to pick`, Nsamples,  `samples`):
		samples := table():
		SOL := table():
		i := 0:
		k := 0:
		while i < Nsamples do
			Q := sampleConfiguration(nops(vars), ran):
			if assigned(SOL[Q]) then
				k := k+1:
			else
				i := i+1:
				samples[i] := Q:
				SOL[Q] := i:
			end if:
		end do:
		userinfo(2, svInt, `...done,`, k, `collisions`):

		# columns are the forms, rows the sample points
		M := LinearAlgebra[Modular][Mod](modulus, [seq([seq(
			wedgeEvalMod(
				op(V[i]),
				[seq(vars[k]=samples[j][k], k=1..nops(vars))]
			, modulus)
		, i=1..nops(V))], j=1..Nsamples)], integer[]):
	end:
	userinfo(4, svInt, `sample matrix:`, print(M)):

	# choose a basis among the simplest elements (greedy):
	userinfo(2, svInt, `starting modular computation of the rank`):
	Q,B,k := LinearAlgebra[Modular][RowEchelonTransform](modulus, M, false, false, false, false):
	dim := nops(B):
	userinfo(2, svInt, ` - rank:`, dim):
	userinfo(2, svInt, ` - selected basis:`, lprint(B)):

	if dim=0 then
		for v in V do
			checkWedgeRelation(v=0):
			WedgeRelations[op(v)] := 0:
		end do:
		return eval(expr, wedge=0):
	end if:
	if dim=nops(V) then
		for v in V do
			WedgeRelations[op(v)] := v:
		end do:
		return expr:
	end if:

	# From Q we can read off a linearly independent set of functionals:
	# reconstruct permutation:
	P := table():
	for i from 1 to Nsamples do
		P[i] := i:
	end do:
	for i from 1 to dim do
		k := P[i]:
		P[i] := P[Q[i]]:
		P[Q[i]] := k:
	end do:
	samples := [seq(samples[P[i]], i=1..dim)]:
	userinfo(4, svInt, `Independent samples:`, lprint(samples)):

	# There is only one case when this is not good enough: Large basis and ResidueSamples = false; in that case, we want to pick a basis of iterated residues to deal with the matrix more easily
	if (not ResidueSamples) and (dim>MAX_DIM_EVAL_SAMPLES) then
		userinfo(2, svInt, `constructing independent iterated residues:`):
		ResidueSamples := true:

		# all singularities in the input: dlog(a-b) -> {a,b}
		# note that we can restrict to the residues from the basis elements; all other residues must vanish
		Poles := map(op, selectfun([seq(V[i], i in B)], dlog)):
		Poles := {seq(seq({i, solve(j,i)}, j in select(has, Poles, i)), i in allvars)}:
		userinfo(2, svInt, ` - poles of basis functions:`, print(Poles)):

		# rewrite dlog(a-b) as dlog({a,b}):
		BasisForms := eval(V, dlog=(foo->dlog({indets(foo)[1], solve(foo, indets(foo)[1])}))):
		userinfo(2, svInt, ` - replaced dlog(a-b) by dlog({a,b})`):

		# sample linearly independent iterated residues:
		samples := table():
		Nsamples := 0:
		RHS := indets(Poles):
		while Nsamples<dim do
			j := 0:
			got := 0:
			while got<`if`(Nsamples=0, dim+10, max(2*(dim-Nsamples), 10, iquo(dim,10))) do
				j := j+1:
				z := sampleResidue(Poles, RHS):
				if not type(z, 'list') then
					next:
				end if:
				# filter out zeros:
				allzero := true:
				for k from 1 to dim do
					if wedgeRes(op(BasisForms[B[k]]), z)<>0 then
						allzero := false:
						break:
					end if:
				end do:
				if allzero then
					next:
				end if:
				got := got+1:
				samples[Nsamples+got] := z:
			end do:
			userinfo(2, svInt, ` - needed`, j,  `samples to generate`, got, `residues`):

			# evaluate residues on basis forms:
			M := LinearAlgebra[Modular][Mod](modulus, [seq([seq(
				wedgeRes(
					op(BasisForms[B[i]]),
					samples[j]
				) mod modulus
			, j=1..Nsamples+got)], i=1..dim)], integer[]):
			userinfo(4, svInt, `residue sample matrix:`, print(M)):

			# Choose basis
			Q := LinearAlgebra[Modular][RankProfile](modulus, M):
			if nops(Q)<Nsamples then
				error "rank decreased":
			end if:
			for i from 1 to Nsamples do
				if Q[i]<>i then
					error "basis element %1 skipped, got %2 instead", i, Q[i]:
				end if:
			end do:
			for i from Nsamples+1 to nops(Q) do
				samples[i] := samples[Q[i]]:
			end do:
			Nsamples := nops(Q):

			userinfo(2, svInt, ` - number of independent residues:`, Nsamples):
		
		end do:

		samples := table([seq(i=samples[i], i=1..Nsamples)]):
		userinfo(4, svInt, `selected independent samples:`, print(op(eval(samples)))):

	end if:

	# evaluate functionals on basis forms:
	userinfo(2, svInt, `creating sample matrix`):
	if ResidueSamples then
		M := Matrix([seq([seq(
			wedgeRes(op(BasisForms[B[i]]), samples[j])
		, i=1..dim)], j=1..dim)]):
	else
		M := Matrix([seq([seq(
			wedgeEval(op(V[B[i]]), [seq(vars[k]=samples[j][k], k=1..nops(vars))])
		, i=1..dim)], j=1..dim)]):
	end if:
	userinfo(4, svInt, `sample matrix:`, print(M)):

	userinfo(2, svInt, `inverting matrix`):
	MM := M^(-1):
	userinfo(4, svInt, `inverse matrix:`, print(M)):

	userinfo(2, svInt, `computing and checking wedge substitutions...`):
	for i from 1 to nops(V) do
		if i in B then
			WedgeRelations[op(V[i])] := V[i]:
			next:
		end if:
		if ResidueSamples then
			RHS := Vector([seq(wedgeRes(op(BasisForms[i]), samples[j]), j=1..dim)]):
		else
			RHS := Vector([seq(wedgeEval(op(V[i]), [seq(vars[k]=samples[j][k], k=1..nops(vars))]), j=1..dim)]):
		end if:
		SOL := MM.RHS:
		userinfo(4, svInt, `solution for`, i, lprint(convert(SOL, 'list'))):
		SUBSTI := V[i] = add(SOL[j]*V[B[j]], j=1..dim):
		checkWedgeRelation(SUBSTI):
		WedgeRelations[op(V[i])] := rhs(SUBSTI):
	end do:
#	userinfo(2, svInt, `performing substitutions of wedges...`):
#	SUBSTI := eval(expr, wedge=(a->WedgeRelations[a])):
	userinfo(2, svInt, `...done`):
#	return SUBSTI:
end proc:

# G = [[e11,e12,...],[e21,e22,...],...,[en1,...]] is an adjacency list
# [e11,e12,...] are the out-neighbours of vertex 1 and so on
weight := proc(G::list(list), tProp::numeric := 1/2)
local Vext, M, e, v, pre, Props, n, m, E, Wedges, tab, resvars, W:
global allvars, delta:
global WedgeRelations:
global FREEZE_WEDGES:

	# reset anything hyperInt has remembered
	forgetAll():

	# check graph
	n := nops(G):
	E := [seq(seq([i, G[i][j]], j=1..nops(G[i])), i=1..n)]:
	m := nops(E)+2-2*n:
	userinfo(1, svInt, `Graph of type [n,m]=`, [n,m]):

	# special cases without propagators:
	if [n,m] in {[1,0],[0,2]} then
		return 1:
	end if:

	if m<2 then
		error "This implementation only works for m>=2. Use 'kontsv.mpl'":
	end if:
	Vext := {seq(op(e), e in G)} minus {seq(1..n)}:
	if not (Vext subset {'L', 'R', seq(cat(p,i), i=1..m)}) then
		error "terrestrial vertices %1 should be labelled L, R, or p1, ..., p%2", Vext, m:
	end if:
	if m>2 then
		error "m>2 not yet implemented. Use 'kontsv.mpl'":
	end if:
	if Vext <> {'L', 'R'} then
		error "terrestrial vertices should be labelled L and R":
	end if:

	# find best order:
	allvars := bestOrder(E):

	userinfo(1, svInt, `Integration order`, allvars):

	# Compute integrand:
	pre := 1/(2*Pi*I)^(2*n): # prefactor in the definition of the weight
	Props := eval(
		[seq(Propagator(e[1], e[2], tProp), e in E)],
		[z['L']=0,zz['L']=0,z['R']=1,zz['R']=1]
	):

	allvars := [seq(op([z[v],zz[v]]), v in allvars)]:
	# sign of imaginary parts:
	for v from 1 to n do
		delta[z[v]] := 1:
		delta[zz[v]] := -delta[z[v]]:
		# for transformation to infinity:
		delta[1/z[v]] := -delta[z[v]]:
		delta[1/zz[v]] := -delta[zz[v]]:
	end do:

	# Integrand as a symbolic wedge of the angle differentials:
	M := wedgeSimplify(Props):
	userinfo(2, svInt, `integrand:`, print(M)):

	# Multiply with a real-valued logarithm to test the algorithms:
	#M := M*fibrationBasis(mul(ln(z[v]-zz[v])-I*Pi/2,v=1..V), allvars):

	Wedges := table():
	for v from 1 to n do
		userinfo(1, svInt, cat(`integration #`, v, `:`), `vertex`, op(allvars[1])):
		pre := pre*(2*I*Pi):

		Wedges[v] := selectfun(M, wedge):
		# make residues in z[v] and zz[v] explicit:
		if nops(allvars)>4 then
			resvars := allvars[1..2]:
		else
			resvars := allvars:
		end if:
		userinfo(1, svInt, `projecting dlog-forms onto residues with respect to`, resvars):
#		M := convert(M, 'IterRes', resvars):
#		M := toWedgeBasis(M):
#		M := svInt(M/(I*Pi*2)):
		toWedgeBasis(map(convert, Wedges[v], 'IterRes', resvars)):
		userinfo(1, svInt, `substituting wedge reductions to basis`):
		for W in Wedges[v] do
			WedgeRelations[op(W)] := eval(convert(W, 'IterRes', resvars), wedge=(a->WedgeRelations[a])):
			if FREEZE_WEDGES then
				WedgeRelations[op(W)] := eval(WedgeRelations[op(W)], wedge=(LL->freeze(wedge(LL)))):
			end if:
		end do:

		userinfo(1, svInt, `collecting Hlog-coefficients into table`):
		tab := table(sparsereduced):
		HlogsIntoTable(M, allvars, tab):
		userinfo(2, svInt, ` - number of terms:`, numelems(tab)):
		userinfo(1, svInt, `applying residue projections to coefficients`):
		for W in {indices(tab, nolist)} do
			tab[W] := eval(expand(tab[W]/(2*I*Pi)), wedge=(a->WedgeRelations[a])):
		end do:
		M := svInt(tab):
		if FREEZE_WEDGES then
			M := thaw(M):
		end if:

		userinfo(4, svInt, `integration result:`, lprint(M)):

		allvars := allvars[3..]:
	end do:
	M := simplify(M*pre):
	userinfo(4, svInt, `final result:`, lprint(M)):
	userinfo(2, svInt, `number of different wedge forms at each integration step:`, [seq(nops(Wedges[v]), v=1..n)]):
	return M:
end proc:

read "graphs/graphs.mpl":
getGraph := proc(hbar::posint, id::posint)
local f, name, di6, i:
	name := cat("graphs/kg", hbar):
	if not FileTools[Exists](name) then
		error "File %1 not found", name:
	end if:
	if id>FileTools[Text][CountLines](name) then
		error "undefined graph %1; there are only %2 at order %3", id, FileTools[Text][CountLines](name), hbar:
	end if:
	for i from 1 to id do
		di6 := FileTools[Text][ReadLine](name):
	end do:
	FileTools[Text][Close](name):
	parseDigraph6(di6):
end proc:

if assigned(hbar) and assigned(g) then
	# read graph from nauty's list:
	G := getGraph(hbar, g):
	print(G):
	# compute and store weight
	if not assigned(t) then
		t := 1/2:
	end if:
	w := weight(G, t):
	save G, w, t, cat("weights/order", hbar, "graph", g):
end if:
