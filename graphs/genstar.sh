#!/bin/bash

# Erik Panzer
# 15 August 2017
# Compute Kontsevich graphs for the star product (m=2 terrestrial vertices on the real line)

#!/bin/bash
if [ ! "$#" -eq "1" ]; then
	echo "Exactly one argument expected: the hbar order (=n=number of internal vertices)"
	exit
fi
hbar="$1"
edges=`echo "2*${hbar}" | bc`
verts=`echo "2+${hbar}" | bc`

nauty="/usr/lib/sagemath/local/bin"

# streamlining: avoids storing the gigantic number of directed graphs on disk!
if [ -f "simple${hbar}" ]; then
	rm "simple${hbar}"
fi
${nauty}/geng -c ${verts} 1:${edges} | #
${nauty}/directg -e${edges} | #
${nauty}/pickg -D2 -M${hbar} -d0 -m2 > "kgdirect${hbar}"
