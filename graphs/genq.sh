#!/bin/bash

# Erik Panzer
# 8 July 2017
# generate only "quadratic" Kontsevich graphs for the star product
if [ ! "$#" -eq "1" ]; then
	echo "Exactly one argument expected: the hbar order (number of internal vertices)"
	exit
fi
hbar="$1"
edges=`echo "2*${hbar}" | bc`
verts=`echo "2+${hbar}" | bc`

nauty="/usr/lib/sagemath/local/bin"

# the initial simple graphs must have:
#   1) exactly 2 vertices of degree 1 (these become L and R with indeg=1)
#   2) all other vertices with degrees in 2..4
${nauty}/geng -c ${verts} 1:${edges} -D4 | #
${nauty}/pickg -d1 -m2 | #
${nauty}/directg -e${edges} | #
${nauty}/pickg -D2 -M${hbar} -d0 -m2 -U:2 > "qkg${hbar}"
